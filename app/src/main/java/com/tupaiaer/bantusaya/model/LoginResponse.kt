package com.tupaiaer.bantusaya.model

data class LoginResponse (
    val jwt: String?
)