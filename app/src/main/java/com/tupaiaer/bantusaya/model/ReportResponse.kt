package com.tupaiaer.bantusaya.model

import com.google.gson.annotations.SerializedName

data class ReportResponse<T> (
    @field:SerializedName("laporans")
    val laporans: T?)
