package com.tupaiaer.bantusaya.model

import com.google.gson.annotations.SerializedName


data class UserList(
    val result: Boolean = false,
    val users: List<UserData>?
)

data class UserData (
    @field:SerializedName("id")
    val id: Int?,
    @field:SerializedName("username")
    val username: String?,
    @field:SerializedName("created_at")
    val createdAt: String?,
    @field:SerializedName("updated_at")
    val updatedAt: String?,
    @field:SerializedName("email")
    val email: String?,
    @field:SerializedName("ages")
    val ages: String?,
    @field:SerializedName("gender")
    val gender: String?,
    @field:SerializedName("user_prev")
    val userPref: String?,
    @field:SerializedName("phoneNumber")
    val phoneNumber: String?,
    @field:SerializedName("address")
    val address: String?,
    @field:SerializedName("birthDate")
    val birthDate: String?
)