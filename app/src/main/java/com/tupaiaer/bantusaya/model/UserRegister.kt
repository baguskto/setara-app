package com.tupaiaer.bantusaya.model

import com.google.gson.annotations.SerializedName

data class UserRegister(
    @field:SerializedName("username")
    val username: String?,
    @field:SerializedName("email")
    val email: String?,
    @field:SerializedName("password")
    val password: String?,
    @field:SerializedName("password_confirmation")
    val passwordConfirmation: String?,
    @field:SerializedName("gender")
    val gender: String?,
    @field:SerializedName("phoneNumber")
    val phoneNumber: String?,
    @field:SerializedName("address")
    val address: String?,
    @field:SerializedName("birthDate")
    val birthDate: String?,
    @field:SerializedName("user_prev")
    val userPrev: String?,
    @field:SerializedName("profil_pic")
    val profilPic: String?,
    @field:SerializedName("identity_pic")
    val identityPic: String?,
    @field:SerializedName("domisili")
    val domisili: String?,
    @field:SerializedName("identity_num")
    val identityNum: String?
)