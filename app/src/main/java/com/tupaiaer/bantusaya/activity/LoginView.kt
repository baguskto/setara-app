package com.tupaiaer.bantusaya.activity

import com.tupaiaer.bantusaya.model.LoginResponse

interface LoginView {
    fun onSuccess(loginResponse: LoginResponse)
    fun onFailure(notif: String)
    fun onTokenExired()
}