package com.tupaiaer.bantusaya.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.tupaiaer.bantusaya.R
import kotlinx.android.synthetic.main.activity_bantuan_faq.*

class BantuanFAQ : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bantuan_faq)

        toolbarConsultation.setNavigationIcon(R.drawable.ic_arrow)
        toolbarConsultation.setNavigationOnClickListener {
            onBackPressed()
        }

        val title:String = intent.getStringExtra("title")
        val desc:String = intent.getStringExtra("desc")

        tvFaqTitle.text = title
        tvFaqDesc.text = desc
    }
}
