package com.tupaiaer.bantusaya.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.tupaiaer.bantusaya.R
import kotlinx.android.synthetic.main.activity_reportation_detail.*

class ReportationDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reportation_detail)

        tbDetailReport3.setNavigationIcon(R.drawable.ic_arrow)
        tbDetailReport3.setNavigationOnClickListener {
            onBackPressed()
        }

        val data1 = intent.getStringExtra("data1")
        val data2 = intent.getStringExtra("data2")
        val data3 = intent.getStringExtra("data3")
        val data4 = intent.getStringExtra("data4")
        val data5 = intent.getStringExtra("data5")
        val data6 = intent.getStringExtra("data6")
        val data7 = intent.getStringExtra("data7")
        val data8 = intent.getStringExtra("data8")
        val data9 = intent.getStringExtra("data9")
        val data10 = intent.getStringExtra("data10")
        val data11 = intent.getStringExtra("data11")
        val data12 = intent.getStringExtra("data12")

        tvIdReportDetail.text = data1
        tvTitleDetail.text = data2
        tvCreatedDetail.text = data5
        tvStatusPelaporDetail.text = data6
        tvNamaPelaporDetail.text = data7
        tvNamaKorbanDetail.text = data9
        tvJenisKelaminDetail.text = data10
        tvUmurKorbanDetail.text = data11
        tvNamaPelakuDetail.text = data7
        tvDeskripsiKejadianDetail.text = data3
        tvLokasiKejadianDetail.text = data8
    }
}
