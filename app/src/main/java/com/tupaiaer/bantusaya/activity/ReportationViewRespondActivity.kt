package com.tupaiaer.bantusaya.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.tupaiaer.bantusaya.BantuSayaApp
import com.tupaiaer.bantusaya.PrefManager
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.adapter.ConvAdapter
import com.tupaiaer.bantusaya.model.ConversationResponse
import com.tupaiaer.bantusaya.model.Reply
import com.tupaiaer.bantusaya.model.Report
import kotlinx.android.synthetic.main.activity_reportation_view_respond.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReportationViewRespondActivity : AppCompatActivity() {

    private val conversationList = mutableListOf<Reply>()
    private val conversationAdapter = ConvAdapter(conversationList, this::onItemClick)
    private lateinit var pref: PrefManager
    var param: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reportation_view_respond)

        tbDetail1.setNavigationIcon(R.drawable.ic_arrow)
        tbDetail1.setNavigationOnClickListener {
            onBackPressed()
        }

        rvConversation.layoutManager = LinearLayoutManager(this)

        pref = PrefManager(this)

        val data1 = intent.getStringExtra("data1")
        val data2 = intent.getStringExtra("data2")
        val data3 = intent.getStringExtra("data3")
        val data4 = intent.getStringExtra("data4")
        val data5 = intent.getStringExtra("data5")
        val data6 = intent.getStringExtra("data6")
        val data7 = intent.getStringExtra("data7")
        val data8 = intent.getStringExtra("data8")
        val data9 = intent.getStringExtra("data9")
        val data10 = intent.getStringExtra("data10")
        val data11 = intent.getStringExtra("data11")
        val data12 = intent.getStringExtra("data12")

        tvReportIdView.text = "LAPORAN ID-" + data1
        tvReportIdView2.text = "LAPORAN ID-" + data1
        tvTitleView.text = data2
        tvDateView.text = data4

        val bundle = Bundle()
        bundle.putString("data1", data1)
        bundle.putString("data2", data2)
        bundle.putString("data3", data3)
        bundle.putString("data4", data4)
        bundle.putString("data5", data5)
        bundle.putString("data6", data6)
        bundle.putString("data7", data7)
        bundle.putString("data8", data8)
        bundle.putString("data9", data9)
        bundle.putString("data10", data10)
        bundle.putString("data11", data11)
        bundle.putString("data12", data12)

        param = data1

        cvDetailReport.setOnClickListener {
            val intent = Intent(this, ReportationDetailActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }

//        cvReply.setOnClickListener {
//            val intent = Intent(this, ReportationReplyActivity::class.java)
//            intent.putExtras(bundle)
//            startActivity(intent)
//        }

        setupView()

    }

    private fun setupView() {

        rvConversation.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = conversationAdapter
        }
        getAllConversation()
    }

    private fun getAllConversation() {
        BantuSayaApp.apiService.getConversation(param)
            .enqueue(object : Callback<ConversationResponse> {
                override fun onFailure(call: Call<ConversationResponse>, t: Throwable) {
                        Log.v("error", t.localizedMessage)
                }

                override fun onResponse(call: Call<ConversationResponse>, response: Response<ConversationResponse>) {
                    val reports: List<Reply>? = response.body()?.replies
                    pref.blankReport = reports.toString()

                    Log.v("reports", pref.blankReport)

                    onResponseSuccess(reports ?: emptyList())
//                    rvConversation.adapter = conversationAdapter
                }
            })
    }

    private fun onResponseSuccess(students: List<Reply>) {
        conversationList.clear()
        conversationList.addAll(students)
        conversationAdapter.notifyDataSetChanged()
    }

    private fun onItemClick(reply: Reply) {
        val bundle = Bundle()

        pref.idConversation = "${reply.conversation_id}"
        pref.idUserConversation = "${reply.user_id}"
        pref.idInstutionConversation = "${reply.institution_id}"
        pref.replyBody = "${reply.reply_body}"
        pref.createdConversation = "${reply.created_at}"

        val intent = Intent(this, ReportationReplyActivity::class.java)
        startActivity(intent)
    }

}
