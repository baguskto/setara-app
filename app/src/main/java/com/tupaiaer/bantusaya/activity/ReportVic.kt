package com.tupaiaer.bantusaya.activity

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import com.tupaiaer.bantusaya.BantuSayaApp
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.model.Report
import com.tupaiaer.bantusaya.utils.toJson
import com.tupaiaer.bantusaya.utils.toast
import kotlinx.android.synthetic.main.activity_report_vic.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.Places
import com.google.android.gms.location.places.ui.PlacePicker
import com.tupaiaer.bantusaya.Constants
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register_step3.*


class ReportVic : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {

    private val tvPlaceDetails: TextView? = null
    private val fabPickPlace: FloatingActionButton? = null
    var fileUri: Uri? = null

    private lateinit var mGoogleApiClient: GoogleApiClient
    private var PLACE_PICKER_REQUEST = 1
    private var latLng = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_vic)

        toolbarReportVic.setNavigationIcon(R.drawable.ic_arrow)
        toolbarReportVic.setNavigationOnClickListener {
            onBackPressed()
        }
        setupView()
    }

    private fun setupView() {

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addApi(Places.GEO_DATA_API)
            .addApi(Places.PLACE_DETECTION_API)
            .enableAutoManage(this, this)
            .build()

        ivMapVic.setOnClickListener {
            val PLACE_PICKER_REQUEST = 1
            val builder = PlacePicker.IntentBuilder()

            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST)
        }

        btnSaveReportVic.setOnClickListener {
            saveReportVic()
            startActivity(Intent(this, ReportResult::class.java))
            finish()
        }

        ivBuktiVic.setOnClickListener {
            pickPhotoFromGallery()
        }
    }

    private fun saveReportVic() {

        val statusPelapor:String = intent.getStringExtra(Constants.STATUS_PELAPOR)
        val kategori:String = intent.getStringExtra(Constants.KATEGORI_LAPOR)
        val status = "pending"

        val report = Report(
            id = null,
            title = kategori,
            description = etDesKejadian.text.toString(),
            status = status,
            statusPelapor = statusPelapor,
            suspect = etSuspect.text.toString(),
            crimeScene = latLng,
            victimAges = null,
            victimAddress = null,
            victimGender = null,
            victimName = null,
            created_at = null
        )

        BantuSayaApp.apiService
            .saveReport(report)
            .enqueue(object : Callback<Any> {
                override fun onFailure(call: Call<Any>, t: Throwable) {
                }

                override fun onResponse(call: Call<Any>, response: Response<Any>) {
                    toast("Laporan berhasil dikirim!")
                    Log.d("MAIN", "response : ${response.body()?.toJson()}")
                }

            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                var place: Place
                place = PlacePicker.getPlace(data, this)
                var stBuilder = StringBuilder()
                var placeName = String.format("%s", place.getName())
                stBuilder.append(placeName)
                ivMapVic.visibility = View.GONE
                tvMapVic.visibility = View.VISIBLE
                tvMapVic.text = stBuilder.toString()
                latLng = placeName
            }
        }
    }


    override fun onConnectionFailed(p0: ConnectionResult) {
//        Snackbar.make(btnLogin, p0.getErrorMessage() + "", Snackbar.LENGTH_LONG).show()
    }

    override fun onStart() {
        super.onStart()
        mGoogleApiClient.connect()
    }

    override fun onStop() {
        mGoogleApiClient.disconnect()
        super.onStop()
    }

    private fun pickPhotoFromGallery() {
        val pickImageIntent = Intent(Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        startActivityForResult(pickImageIntent, Constants.PICK_PHOTO_REQUEST)
    }

}
