package com.tupaiaer.bantusaya.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.adapter.PagerAdapter
import kotlinx.android.synthetic.main.activity_consultation.*
import kotlinx.android.synthetic.main.activity_consultation.view.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_report.*

class ConsultationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_consultation)

        toolbarConsultation.setNavigationIcon(R.drawable.ic_arrow)
        toolbarConsultation.setNavigationOnClickListener {
            onBackPressed()
        }

        viewPager.adapter = PagerAdapter(supportFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
    }
}
