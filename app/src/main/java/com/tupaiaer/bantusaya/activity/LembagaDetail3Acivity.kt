package com.tupaiaer.bantusaya.activity

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.tupaiaer.bantusaya.R
import kotlinx.android.synthetic.main.activity_lembaga_detail.*

class LembagaDetail3Acivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lembaga_detail3_acivity)
        cvPhone.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "081223090469"))
            startActivity(intent)
        }

        cvWhatsapp.setOnClickListener {
            openWhatsApp()
        }
    }

    fun openWhatsApp() {
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("https://api.whatsapp.com/send?phone=62895606155325&text=Hello%21%0aKak%20curhat%20dong..&source=&data=")
            startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
