package com.tupaiaer.bantusaya.activity

import com.tupaiaer.bantusaya.data.ApiService
import com.tupaiaer.bantusaya.model.LoginResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter(val apiService: ApiService, val loginView: LoginView) {

    fun login(req: Any) {
        apiService.loginUser(req)
            .enqueue(object : Callback<LoginResponse> {
                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                }

                override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                    if (response.isSuccessful) {
                        response.body()?.let {

                            if (it.jwt.isNullOrBlank()) {
                                loginView.onFailure("Gagal login")
                            }else {
                                loginView.onSuccess(it)

                            }
                        }
                    } else if (response.code() == 401) {
                        loginView.onTokenExired()
                    }
                    else {
                        loginView.onFailure("Username atau password salah!")
                    }
                }

            })
    }
}