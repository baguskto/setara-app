package com.tupaiaer.bantusaya.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.tupaiaer.bantusaya.BantuSayaApp
import com.tupaiaer.bantusaya.PrefManager
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.model.LoginResponse
import com.tupaiaer.bantusaya.model.ProfileResponse
import com.tupaiaer.bantusaya.model.User
import com.tupaiaer.bantusaya.utils.toJson
import com.tupaiaer.bantusaya.utils.toast
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.google.android.gms.location.places.Places
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlacePicker


class LoginActivity : AppCompatActivity(), LoginView {

    private lateinit var pref: PrefManager
    private lateinit var loginPresenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        toolbarLogin.setNavigationIcon(R.drawable.ic_arrow_back_black)
        toolbarLogin.setNavigationOnClickListener {
            onBackPressed()
        }

        loginPresenter = LoginPresenter(BantuSayaApp.apiService, this)
        pref = PrefManager(this)

        setupView()

    }

    private fun setupView() {

        btnLogin.setOnClickListener {
            login()
        }

        tvRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivityStep1::class.java))
        }

        tvForgetPass.setOnClickListener {
            startActivity(Intent(this, ForgotPassActivity::class.java))
        }
    }

    private fun login() {
        val email = etEmailLogin.text.toString()
        val password = etPasswordLogin.text.toString()

        if (email.isBlank()) {
            etEmailLogin.setError("Email / Nomor Handphone wajib diisi")
        } else if (password.isBlank()) {
            etPasswordLogin.setError("Password wajib diisi")
        }

        val loginMap = mutableMapOf<String, String>()
        loginMap["email"] = email
        loginMap["password"] = password

        val map = mapOf("auth" to loginMap)
        Log.d("LOIN", "map : ${map.toJson()}")

        showProgress(true)
        loginPresenter.login(map)
    }

    private fun goToMain() {
        pref.hasLogin = true
        val email = etEmailLogin.text.toString()

        if (email == "adminsetara@gmail.com" || pref.name == "adminsetara") {
            val gotoMain = Intent(this, AdminReportationStatusActivity::class.java)
            startActivity(gotoMain)
            finish()
        } else {
            val gotoMain = Intent(this, MainActivity::class.java)
            startActivity(gotoMain)
            finish()
        }
    }

    private fun getUser() {
        BantuSayaApp.apiService.userProfile()
            .enqueue(object : Callback<ProfileResponse> {
                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    Log.v("throwable", t.localizedMessage)
                }

                override fun onResponse(call: Call<ProfileResponse>, response: Response<ProfileResponse>) {
                    val id = response.body()?.user?.id
                    val username = response.body()?.user?.username
                    val tglLahir = response.body()?.user?.birthDate
                    val alamat = response.body()?.user?.address
                    val email = response.body()?.user?.email
                    val noHp = response.body()?.user?.phoneNumber
                    pref.idUser = id.toString()
                    pref.name = username
                    pref.birthdate = tglLahir
                    pref.address = alamat
                    pref.email = email
                    pref.phone = noHp
                }

            })
    }

    override fun onSuccess(loginResponse: LoginResponse) {
        pref.token = loginResponse.jwt

        showProgress(true)
        if (pref.token.isNullOrBlank()) {
            showProgress(false)
            Snackbar.make(
                rootLoginActivity, // Parent view
                "Maaf! gagal login", // Message to show
                Snackbar.LENGTH_SHORT // How long to display the message.
            ).show()        }
            else {
            pref.token = loginResponse.jwt
            getUser()
            showProgress(false)
            goToMain()
        }
    }

    override fun onFailure(notif: String) {
        toast(notif)
        Snackbar.make(rootLoginActivity, "Username atau password salah!", Snackbar.LENGTH_SHORT).show()
        showProgress(false)
    }

    override fun onTokenExired() {

    }

    private fun showProgress(show: Boolean) {
        pbLogin?.visibility = if (show) View.VISIBLE else View.GONE
    }
}