package com.tupaiaer.bantusaya.activity

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.places.Places
import com.tupaiaer.bantusaya.BantuSayaApp
import com.tupaiaer.bantusaya.ConnectionReceiver
import com.tupaiaer.bantusaya.Constants.STATUS_PELAPOR
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.utils.toast
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register_step1.*
import kotlinx.android.synthetic.main.activity_report.*

class ReportActivity : AppCompatActivity() {

    var statusPelapor: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report)

        toolbarReport.setNavigationIcon(R.drawable.ic_arrow)
        toolbarReport.setNavigationOnClickListener {
            onBackPressed()
        }

        cvKorban.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cvKorban.backgroundTintList = getColorStateList(R.color.colorPrimary)
                txtKorban.setTextColor(ContextCompat.getColor(this, R.color.white))
                cvSaksi.backgroundTintList = getColorStateList(R.color.white)
                txtSaksi.setTextColor(ContextCompat.getColor(this, R.color.black))
            }

            cvSaksi.isSelected = false
            statusPelapor = "Korban"
            Log.v("korban", statusPelapor)
        }

        cvSaksi.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cvSaksi.backgroundTintList = getColorStateList(R.color.colorPrimary)
                txtSaksi.setTextColor(ContextCompat.getColor(this, R.color.white))
                cvKorban.backgroundTintList = getColorStateList(R.color.white)
                txtKorban.setTextColor(ContextCompat.getColor(this, R.color.black))
            }

            cvKorban.isSelected = false
            statusPelapor = "Saksi"
            Log.v("saksi", statusPelapor)
        }

        btnLanjutReport.setOnClickListener {
            val bundle = Bundle()

            if (statusPelapor == "Korban") {
                bundle.putString(STATUS_PELAPOR, statusPelapor)
                val intent = Intent(this, ReportCategory::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            } else if (statusPelapor == "Saksi") {
                bundle.putString(STATUS_PELAPOR, statusPelapor)
                val intent = Intent(this, ReportCategory::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }
    }
}
