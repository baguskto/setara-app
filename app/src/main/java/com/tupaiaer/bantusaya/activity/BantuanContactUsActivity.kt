package com.tupaiaer.bantusaya.activity

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.tupaiaer.bantusaya.R
import kotlinx.android.synthetic.main.activity_contact_us.*

class BantuanContactUsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)

        toolbarContactUs.setNavigationIcon(R.drawable.ic_arrow)
        toolbarContactUs.setNavigationOnClickListener {
            onBackPressed()
        }

        cvDialAdmin.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "0274562714"))
                    startActivity (intent)
        }

    }
}
