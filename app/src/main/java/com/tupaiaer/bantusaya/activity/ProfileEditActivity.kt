package com.tupaiaer.bantusaya.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.text.Editable
import com.tupaiaer.bantusaya.PrefManager
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.utils.toast
import kotlinx.android.synthetic.main.activity_profile_edit.*
import kotlinx.android.synthetic.main.cast_expanded_controller_activity.*

class ProfileEditActivity : AppCompatActivity() {

    private lateinit var pref: PrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_edit)

        toolbarEditProfile.setNavigationIcon(R.drawable.ic_arrow)
        toolbarEditProfile.setNavigationOnClickListener {
            onBackPressed()
        }

        btnSaveProfile.setOnClickListener {
            Snackbar.make(rootProfileEdit, "Fitur mendatang:)", Snackbar.LENGTH_SHORT).show()
        }
        pref = PrefManager(this)
        fun String.toEditable(): Editable =  Editable.Factory.getInstance().newEditable(this)

        etNameSetting.text = pref.name?.toEditable()
        etBirthdateSetting.text = pref.birthdate?.toEditable()
        etAddressSetting.text = pref.address?.toEditable()
        etEmailSetting.text = pref.address?.toEditable()
        etPhoneSettings.text = pref.phone?.toEditable()
    }
}
