package com.tupaiaer.bantusaya.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.tupaiaer.bantusaya.PrefManager
import com.tupaiaer.bantusaya.R
import kotlinx.android.synthetic.main.activity_admin_reportation_reply.*
import kotlinx.android.synthetic.main.activity_reportation_reply.*

class AdminReportationReplyActivity : AppCompatActivity() {

    private lateinit var pref: PrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_reportation_reply)

        pref = PrefManager(this)

        tvUserIdConversationDetailAdmin.text = pref.name
        tvReplyBodyConversationDetailAdmin.text = pref.replyBody
        tvDateConversationDetailAdmin.text = pref.createdConversation

        btnReplyConversationAdmin.setOnClickListener {
            startActivity(Intent(this, ReportationWriteReplyActivity::class.java))
        }
    }
}
