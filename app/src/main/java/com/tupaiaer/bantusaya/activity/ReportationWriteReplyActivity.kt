package com.tupaiaer.bantusaya.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import com.tupaiaer.bantusaya.BantuSayaApp
import com.tupaiaer.bantusaya.Constants
import com.tupaiaer.bantusaya.PrefManager
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.model.Reply
import com.tupaiaer.bantusaya.model.Report
import com.tupaiaer.bantusaya.model.SendReply
import com.tupaiaer.bantusaya.utils.toJson
import com.tupaiaer.bantusaya.utils.toast
import kotlinx.android.synthetic.main.activity_reportation_write_reply.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReportationWriteReplyActivity : AppCompatActivity() {

    private lateinit var pref: PrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reportation_write_reply)

        pref = PrefManager(this)
        Log.v("id", "${pref.idConversation}")

        btnSendReplyConversation.setOnClickListener {
            saveRepliesUser()
        }
    }

    private fun saveRepliesUser() {

        val conversationId = "${pref.idConversation}".toInt()

        val report = SendReply(
            conv_id = conversationId,
            reply_body = etReply.text.toString()
        )

        BantuSayaApp.apiService
            .saveReplies(report)
            .enqueue(object : Callback<Any> {
                override fun onFailure(call: Call<Any>, t: Throwable) {
                }

                override fun onResponse(call: Call<Any>, response: Response<Any>) {
                    Snackbar.make(rootReportationWrite, "Berhasil mengirim pesan!", Snackbar.LENGTH_SHORT).show()
                    finish()
                    Log.d("MAIN", "response : ${response.body()?.toJson()}")
                }

            })
    }
}
