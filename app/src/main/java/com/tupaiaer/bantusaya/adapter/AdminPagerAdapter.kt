package com.tupaiaer.bantusaya.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.tupaiaer.bantusaya.fragment.PendingFragment
import com.tupaiaer.bantusaya.fragment.ProgressFragment
import com.tupaiaer.bantusaya.fragment.TeratasiFragment

class AdminPagerAdapter(fm: FragmentManager): FragmentPagerAdapter(fm){

    private val pages = listOf(
        PendingFragment(),
        ProgressFragment(),
        TeratasiFragment()
    )

    override fun getItem(p0: Int): Fragment {
        return pages[p0]
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Pending"
             1 -> "Progress"
             else-> "Teratasi"

        }
    }
}