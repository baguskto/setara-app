package com.tupaiaer.bantusaya.adapter

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.model.Report
import kotlinx.android.synthetic.main.report_item.view.*

class ReportAdapter(
    private val reportList: List<Report>,
    private val onClickListener: (report: Report) -> Unit
) : RecyclerView.Adapter<ReportAdapter.ReportHolder>() {

    override fun onCreateViewHolder(group: ViewGroup, viewType: Int): ReportHolder {
        val inflater = LayoutInflater.from(group.context)
        val view: View = inflater.inflate(R.layout.report_item, group, false)
        return ReportHolder(view)
    }

    override fun getItemCount(): Int = reportList.size

    override fun onBindViewHolder(holder: ReportHolder, i: Int) {
        val report = reportList[i]
        holder.bind(report)

        holder.itemView.setOnClickListener {
            onClickListener(report)
        }
    }

    inner class ReportHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(report: Report) = with(itemView) {
            tvReportId.text = "LAPORAN  ID-" + report.id
            tvReportCategory.text = report.title
            tvReportStatus.text = report.status
//            val userId = report.user_id
            tvReportDate.text = report.created_at
//            val updatedAt = report.updated_at
            val statusPelapor = report.statusPelapor

            if (report.status == "progress" || report.status == "Progress") {
                tvReportStatus.setBackgroundResource(R.drawable.ic_rectangle_progress)
                tvReportStatus.setTextColor(ContextCompat.getColor(context, R.color.white))
            } else if (report.status == "solved" || report.status == "Solved") {
                tvReportStatus.setBackgroundResource(R.drawable.ic_rectangle_solved)
                cvReportItem.setCardBackgroundColor(ContextCompat.getColor(context, R.color.green2))
                tvReportCategory.setTextColor(ContextCompat.getColor(context, R.color.white))
                tvReportDate.setTextColor(ContextCompat.getColor(context, R.color.white))
                tvReportId.setTextColor(ContextCompat.getColor(context, R.color.white))
            }
        }
    }
}