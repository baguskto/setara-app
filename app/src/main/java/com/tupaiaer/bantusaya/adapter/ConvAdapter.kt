package com.tupaiaer.bantusaya.adapter

import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tupaiaer.bantusaya.PrefManager
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.model.ConversationResponse
import com.tupaiaer.bantusaya.model.Reply
import com.tupaiaer.bantusaya.model.Report
import kotlinx.android.synthetic.main.activity_reportation_reply.view.*
import kotlinx.android.synthetic.main.conversation_item.view.*
import kotlinx.android.synthetic.main.report_item.view.*

class ConvAdapter(private val reportList: List<Reply>, private val onClickListener: (report: Reply) -> Unit) : RecyclerView.Adapter<ConvAdapter.ConvHolder>() {

    private lateinit var pref: PrefManager

    override fun onCreateViewHolder(group: ViewGroup, viewType: Int): ConvHolder {
        val inflater = LayoutInflater.from(group.context)
        val view: View = inflater.inflate(R.layout.conversation_item, group, false)

        pref = PrefManager(view.context)
        return ConvHolder(view)
    }

    override fun getItemCount(): Int = reportList.size

    override fun onBindViewHolder(holder: ConvHolder, i: Int) {
        val report = reportList[i]
        holder.bind(report)

        holder.itemView.setOnClickListener {
            onClickListener(report)
        }
    }

    inner class ConvHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(convResponse: Reply) = with(itemView) {
            if (convResponse.institution_id == null) {
                tvUserIdConversation.text = "${convResponse.user_id.username}"
            } else if (convResponse.user_id == null) {
                tvUserIdConversation.text = "${convResponse.user_id.username}"
            }

            Log.v("testtest", tvUserIdConversation.text.toString())

            if (tvUserIdConversation.text.toString() == pref.name) {
                ivMe.visibility = View.VISIBLE
            } else {
                ivMe.visibility = View.GONE
            }

            tvReplyBodyConversation.text = convResponse.reply_body
        }
    }
}