package com.tupaiaer.bantusaya.data

import com.tupaiaer.bantusaya.model.LoginResponse
import com.tupaiaer.bantusaya.model.Report
import com.tupaiaer.bantusaya.model.User
import retrofit2.Call
import retrofit2.http.*

interface ApiTwitterService {

    @POST("signup")
    fun saveUser(@Body map: Map<String, User>): Call<Any>

    @POST("auth/login")
    fun loginUser(@Body map: Any): Call<LoginResponse>

    @POST("profile")
    fun userProfile(): Call<LoginResponse>

    @POST("laporans")
    fun saveReport(@Body map: Map<String, Report>): Call<Any>
}