package com.tupaiaer.bantusaya.data

import android.content.Context
import android.content.SharedPreferences
import com.tupaiaer.bantusaya.Constants

class PrefManager(context: Context) {

    private val sp: SharedPreferences by lazy {
        context.applicationContext.getSharedPreferences(Constants.SP_PREF_NAME, Context.MODE_PRIVATE)
    }

    private val spe: SharedPreferences.Editor by lazy {
        sp.edit()
    }

    var hasIntro: Boolean
        get() = sp.getBoolean(Constants.SP_HAS_INTRO_SLIDER, false)
        set(value) = spe.putBoolean(Constants.SP_HAS_INTRO_SLIDER, value).apply()

    var token: String?
        get() = sp.getString(Constants.SP_TOKEN, "")
        set(value) {
            spe.putString(Constants.SP_TOKEN, value)
            spe.apply()
        }
}