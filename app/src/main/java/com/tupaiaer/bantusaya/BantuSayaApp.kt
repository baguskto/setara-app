package com.tupaiaer.bantusaya

import android.app.Application
import com.tupaiaer.bantusaya.data.ApiService
import com.tupaiaer.bantusaya.data.ApiTwitterService
import okhttp3.Connection
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class BantuSayaApp : Application() {

    companion object {
        @get:Synchronized
        lateinit var instance: BantuSayaApp
        lateinit var apiService: ApiService
        lateinit var prefManager: PrefManager

    }

    private var token = ""

    override fun onCreate() {
        super.onCreate()
//
        instance = this
        prefManager = PrefManager(this)
        setupRetrofit()
    }

    private fun setupRetrofit() {
        val studentRetrofit = Retrofit.Builder                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ()
            .baseUrl("https://setara-id.herokuapp.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(getOkHttpClient())
            .build()

        apiService = studentRetrofit.create(ApiService::class.java)

        ////////////////////////

//        val twitterRetrofit = Retrofit.Builder()
//            .baseUrl("")
//            .addConverterFactory(GsonConverterFactory.create())
//            .client(getOkHttpClientTwitter())
//            .build()
//
//        apiTwitterService = twitterRetrofit.create(ApiTwitterService::class.java)
    }

    fun setConnectionListener(listener: ConnectionReceiver.ConnectionReceiverListener) {
        ConnectionReceiver.connectionReceiverListener = listener
    }

    private fun getOkHttpClient(): OkHttpClient {
        val timeOut = 60L
        return OkHttpClient.Builder()
            .connectTimeout(timeOut, TimeUnit.SECONDS)
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .writeTimeout(timeOut, TimeUnit.SECONDS)
            .addInterceptor(getInterceptor())
            .addInterceptor {
                val req = it.request()
                    .newBuilder()
                    .addHeader("Authorization", prefManager.token)
                    .build()
                return@addInterceptor it.proceed(req)
            }
            .build()
    }

    private fun getOkHttpClientTwitter(): OkHttpClient {
        val timeOut = 60L
        return OkHttpClient.Builder()
            .connectTimeout(timeOut, TimeUnit.SECONDS)
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .writeTimeout(timeOut, TimeUnit.SECONDS)
            .addInterceptor(getInterceptor())
            .addInterceptor {
                val req = it.request()
                    .newBuilder()
                    .addHeader("Authorization", token)
                    .addHeader("Content-Type:","application/json")
                    .build()
                return@addInterceptor it.proceed(req)
            }
            .build()
    }

    private fun getInterceptor(): Interceptor {
        return HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.NONE
            }
        }
    }
}