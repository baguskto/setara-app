package com.tupaiaer.bantusaya.fragment


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tupaiaer.bantusaya.BantuSayaApp
import com.tupaiaer.bantusaya.PrefManager

import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.activity.AdminReportationUpdateStatusActivity
import com.tupaiaer.bantusaya.activity.ReportationViewRespondActivity
import com.tupaiaer.bantusaya.adapter.AdminPendingReportAdapter
import com.tupaiaer.bantusaya.model.*
import kotlinx.android.synthetic.main.fragment_admin_pending.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class PendingFragment : Fragment() {

    private val reportList = mutableListOf<Laporan>()
    private val reportAdapter = AdminPendingReportAdapter(reportList, this::onItemClick)
    private lateinit var pref: PrefManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_admin_pending, container, false)

        pref = PrefManager(activity!!)

        if (pref.blankReport.isNullOrBlank()) {
            view.ivBlankReport.visibility = View.VISIBLE
            view.tvBlankReport.visibility = View.VISIBLE
            view.rvPendingReport.visibility = View.GONE
        }
        else {
            view.rvPendingReport.visibility = View.VISIBLE
            view.ivBlankReport.visibility = View.GONE
            view.tvBlankReport.visibility = View.GONE
        }

        return view
    }

    companion object {
        fun newInstance(): PendingFragment {
            val fragment = PendingFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
    }

    private fun setupView() = view?.apply {

        rvPendingReport.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = reportAdapter
        }
        getAllPendingReport()
    }

    private fun onItemClick(report: Laporan) {
        val bundle = Bundle()
        bundle.putString("data1", "${report.id}")
        bundle.putString("data2", report.title)
        bundle.putString("data3", report.description)
        bundle.putString("data4", report.status)
        bundle.putString("data5", "${report.user_id}")
        bundle.putString("data6", report.created_at)
        bundle.putString("data7", report.updated_at)
        bundle.putString("data8", "${report.suspect}")
        bundle.putString("data9", "${report.crime_scene}")
        bundle.putString("data10", report.statusPelapor)
        bundle.putString("data11", "${report.victim_name}")
        bundle.putString("data12", "${report.victim_gender}")
        bundle.putString("data13", "${report.victim_ages}")
        bundle.putString("data14", "${report.victim_address}")

        val intent = Intent(this.activity, AdminReportationUpdateStatusActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private fun getAllPendingReport() {
        BantuSayaApp.apiService.getAllReportUser()
            .enqueue(object : Callback<ReportAllUserResponse> {
                override fun onFailure(call: Call<ReportAllUserResponse>, t: Throwable) {
                    Log.v("error", t.localizedMessage)
                }

                override fun onResponse(call: Call<ReportAllUserResponse>, response: Response<ReportAllUserResponse>) {
                    val reports: List<Laporan>? = response.body()?.laporans
                    pref.blankReport = reports.toString()

                    Log.v("report", pref.blankReport)
                    onResponseSuccess(reports ?: emptyList())
                }
            })
    }

    private fun onResponseSuccess(students: List<Laporan>) {
        reportList.clear()
        reportList.addAll(students)
        reportAdapter.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        getAllPendingReport()
    }
}
