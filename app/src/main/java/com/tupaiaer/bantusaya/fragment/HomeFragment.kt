package com.tupaiaer.bantusaya.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tupaiaer.bantusaya.BantuSayaApp
import com.tupaiaer.bantusaya.PrefManager

import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.activity.*
import com.tupaiaer.bantusaya.model.ProfileResponse
import kotlinx.android.synthetic.main.fragment_home.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment : Fragment() {

    private lateinit var pref: PrefManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_home, container, false)

        pref = PrefManager(activity!!)
        getUser()

        if (pref.name.isNullOrBlank()) {
            view.tvLogin.visibility = View.VISIBLE
            view.tvNameUser.visibility = View.INVISIBLE
        }

        else if (pref.name != null) {
            view.tvLogin.visibility = View.INVISIBLE
            view.tvNameUser.text = "Hai " + pref.name + "!"
        }

        view.tvLogin.setOnClickListener {
            startActivity(Intent(this.activity, LoginActivity::class.java))
        }

        view.ibReport.setOnClickListener {
            if (pref.name.isNullOrBlank()) {
                startActivity(Intent(this.activity, LoginActivity::class.java))
            } else {
                startActivity(Intent(this.activity, ReportActivity::class.java))
            }
        }

        view.ibConsul.setOnClickListener {
            if (pref.name.isNullOrBlank()) {
                startActivity(Intent(this.activity, LoginActivity::class.java))
            } else {
                startActivity(Intent(this.activity, ConsultationActivity::class.java))
            }
        }

        view.ibNews.setOnClickListener {
                startActivity(Intent(this.activity, NewsActivity::class.java))
        }

        view.tvLihatSemua.setOnClickListener {
            startActivity(Intent(this.activity, NewsReadActivity::class.java))
        }

        view.cvNewsHome.setOnClickListener {
            startActivity(Intent(this.activity, NewsReadActivity::class.java))
        }

        view.cvNewsHome2.setOnClickListener {
            startActivity(Intent(this.activity, NewsRead2Activity::class.java))
        }

            return view
    }

    private fun getUser() {
        BantuSayaApp.apiService.userProfile()
            .enqueue(object : Callback<ProfileResponse> {
                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    Log.v("throwable", t.localizedMessage)
                }

                override fun onResponse(call: Call<ProfileResponse>, response: Response<ProfileResponse>) {
                    val id = response.body()?.user?.id
                    val username = response.body()?.user?.username
                    val tglLahir = response.body()?.user?.birthDate
                    val alamat = response.body()?.user?.address
                    val email = response.body()?.user?.email
                    val noHp = response.body()?.user?.phoneNumber
                    pref.idUser = id.toString()
                    pref.name = username
                    pref.birthdate = tglLahir
                    pref.address = alamat
                    pref.email = email
                    pref.phone = noHp
                }

            })
    }

    companion object {
        fun newInstance(): HomeFragment {
            val fragment = HomeFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}