package com.tupaiaer.bantusaya.fragment


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.activity.LembagaDetail2Acivity
import com.tupaiaer.bantusaya.activity.LembagaDetailActivity
import kotlinx.android.synthetic.main.fragment_psychology.*
import kotlinx.android.synthetic.main.fragment_psychology.view.*

class PsychologyFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_psychology, container, false)

        view.cvPsiko1.setOnClickListener {
            startActivity(Intent(this.activity, LembagaDetailActivity::class.java))
        }
        view.cvPsiko2.setOnClickListener {
            startActivity(Intent(this.activity, LembagaDetail2Acivity::class.java))
        }
        return view
    }


}
