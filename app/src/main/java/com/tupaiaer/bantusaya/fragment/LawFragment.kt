package com.tupaiaer.bantusaya.fragment


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.activity.LembagaDetail3Acivity
import com.tupaiaer.bantusaya.activity.LembagaDetail4Acivity
import com.tupaiaer.bantusaya.activity.LembagaDetailActivity
import kotlinx.android.synthetic.main.fragment_law.view.*
import kotlinx.android.synthetic.main.fragment_psychology.*
import kotlinx.android.synthetic.main.fragment_psychology.view.*

class LawFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_law, container, false)

        view.cvLaw1.setOnClickListener {
            startActivity(Intent(this.activity, LembagaDetail3Acivity::class.java))
        }
        view.cvLaw2.setOnClickListener {
            startActivity(Intent(this.activity, LembagaDetail4Acivity::class.java))
        }
        return view
    }


}
