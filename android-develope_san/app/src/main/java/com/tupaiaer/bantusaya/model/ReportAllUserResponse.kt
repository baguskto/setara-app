package com.tupaiaer.bantusaya.model

data class ReportAllUserResponse(
    val laporans: List<Laporan>,
    val result: Boolean
)

data class Laporan(
    val id: Int,
    val title: String,
    val description: String,
    val status: String,
    val user_id: Int,
    val created_at: String,
    val updated_at: String,
    val suspect: Any,
    val crime_scene: Any,
    val statusPelapor: String,
    val victim_name: Any,
    val victim_gender: Any,
    val victim_ages: Any,
    val victim_address: Any
)

