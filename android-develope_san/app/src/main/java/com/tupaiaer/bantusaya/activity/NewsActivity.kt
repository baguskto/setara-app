package com.tupaiaer.bantusaya.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.tupaiaer.bantusaya.R
import kotlinx.android.synthetic.main.activity_news.*

class NewsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)

        toolbarDetailNews.setNavigationIcon(R.drawable.ic_arrow)
        toolbarDetailNews.setNavigationOnClickListener {
            onBackPressed()
        }

        cvBeritaUtama.setOnClickListener {
            startActivity(Intent(this, NewsReadActivity::class.java))
        }

        cvBeritaPelengkap.setOnClickListener {
            startActivity(Intent(this, NewsRead2Activity::class.java))
        }

    }
}
