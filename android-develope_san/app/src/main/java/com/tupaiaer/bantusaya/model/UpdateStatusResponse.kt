package com.tupaiaer.bantusaya.model

data class UpdateStatusResponse(
    val laporan: Laporan,
    val result: Boolean
)