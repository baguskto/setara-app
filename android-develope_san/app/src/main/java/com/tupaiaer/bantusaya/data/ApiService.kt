package com.tupaiaer.bantusaya.data

import com.tupaiaer.bantusaya.model.*
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @POST("signup")
    @Headers("Content-Type: application/json")
    fun saveUser(@Body map: Any): Call<Any>

    @POST("auth/login")
    @Headers("Content-Type: application/json")
    fun loginUser(@Body map: Any): Call<LoginResponse>

    @GET("profile")
    @Headers("Content-Type: application/json")
    fun userProfile(): Call<ProfileResponse>

    @GET("replies/showReplies/{id}")
    @Headers("Content-Type: application/json")
    fun getConversation(@Path("id") reportId: String): Call<ConversationResponse>

    @GET("laporan/mylaporan")
    @Headers("Content-Type: application/json")
    fun allReport(): Call<ReportResponse<List<Report>>>

    @GET("laporans")
    @Headers("Content-Type: application/json")
    fun getAllReportUser(): Call<ReportAllUserResponse>

    @POST("replies/byUser")
    @Headers("Content-Type: application/json")
    fun saveReplies(@Body map: Any): Call<Any>

    @POST("replies/byInstution")
    @Headers("Content-Type: application/json")
    fun saveRepliesAdmin(@Body map: Any): Call<Any>

    @POST("laporans")
    @Headers("Content-Type: application/json")
    fun saveReport(@Body map: Any): Call<Any>

    @PUT("api/v1/student/{id}")
    @Headers("Content-Type: application/json")
    fun updateStatus(@Path("id") statusPelapor: String): Call<UpdateStatusResponse>

}