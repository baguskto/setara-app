package com.tupaiaer.bantusaya.fragment


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.activity.BantuanContactUsActivity
import com.tupaiaer.bantusaya.activity.BantuanFAQ
import kotlinx.android.synthetic.main.fragment_bantuan.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class BantuanFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_bantuan, container, false)

        val title1 = "Apa itu SETARA?"
        val desc1 = "SETARA adalah layanan berbasis aplikasi android yang mencoba menjawab kebutuhan perempuan dan anak korban kekerasan. SETARA menyediakan layanan lapor kekerasan, layanan konsultasi, dan artikel informatif terkait perempuan dan anak dalam fitur berita. \n" +
                "Laporan yang kami terima nantinya akan dibaca oleh Badan Pemberdayaan Perempuan dan Masyarakat DIY selaku Admin. Kemudian laporan akan dirujuk ke mitra Lembaga  kami yang berpengalaman menangani kasus sesuai kebutuhan pelapor. Sedangkan melalui fitur konsultasi, kamu bisa  menanyakan beberapa hal yang berhubungan dengan kekerasan yang kamu lihat atau alami. Fitur berita mencoba memberikan informasi terkait kekerasan, dari jenis-jenis kekerasaan, hingga bagaimana penanganan yang tepat dalam kasus kekerasan pada perempuan dan anak. \n"

        val title2 = "Apa yang perlu kamu ketahui sebelum kamu melapor?"
        val desc2 = "Setelah memberikan laporan, kamu akan diarahkan untuk berhubungan dengan Lembaga yang akan menangani kasus kamu. Data dan laporan yang kamu berikan akan dijaga kerahasiaannya. Lembaga yang akan menangani kamu adalah lembaga-lembaga yang sudah dipilih SETARA. Lembaga-lembaga tersebut adalah lembaga yang secara profesional menangani laporan kekerasan pada perempuan dan anak. Sebagian besar dari lembaga tersebut juga tergabung dalam Forum Penanganan Korban Kekerasan terhadap Perempuan dan Anak (FPK2PA). \n" +
                "Dalam mengisi form pelaporan, deskripsi yang lengkap membantu kami untuk menentukan lembaga mana yang cocok untuk menangani laporanmu. Jadi sebelum melapor cobalah untuk mencari tempat yang aman dan cobalah untuk menenangkan diri agar kamu bisa melapor dengan baik.\n"

        val title3 = "Apa perbedaan Fitur Lapor dan Fitur Konsultasi?"
        val desc3 ="Fitur lapor merupakan wadah bercerita bagi kamu yang belum mengetahui kebutuhanmu terkait kasus yang kamu alami. Sehingga proses penindaklanjutan dan pemulihan atas kasus dapat segera kamu akses dengan mudah. Dengan fitur ini kamu akan mendapat arahan dan pendampingan hingga pulih\n" +
                "\n" +
                "Fitur konsultasi adalah wadah bagi kamu yang sudah menyadari kebutuhan spesifik atas kasus yang kamu hadapi, baik kebutuhan informasi mengenai hukum maupun psikologi. Dengan fitur cepat tanggap efektif ini kamu akan mendapatkan kesempatan untuk bercerita yang nyaman dan solutif dari mitra tenaga ahli yang berkompeten di bidangnya.\n"

        val title4 = "Apakah saya bisa konsultasi tanpa harus melapor?\n"
        val desc4 ="Kamu bisa berkonsultasi ke lembaga-lembaga yang ada dalam daftar konsultasi. Konsultasi akan dilakukan via WhatsApp dan akan dilayani berdasarkan kemampuan lembaga. Konsultasi akan dilayani dalam jam layanan.\n"

        val title5 = "Siapa yang menerima dan menindaklanjuti laporan saya?\n"
        val desc5 ="Laporan kamu akan diterima dan ditindaklanjuti oleh Badan Pemberdayaan Perempuan dan Masyarakat DIY selaku Admin. Admin akan mengarahkan kamu ke Lembaga yang sesuai dengan kebutuhanmu. Lembaga-lembaga tersebut yang akan menangani ataupun mendampingi kamu sampai kamu pulih."

        val title6 = "Apa yang harus dilakukan setelah melakukan laporan?\n"
        val desc6 ="Setelah mengirim laporan, kamu akan secepatnya diberi arahan oleh Admin untuk kemudian didampingi Lembaga yang ditunjuk. Setelah mendapat arahan dari Admin, buatlah janji dengan Lembaga untuk bertemu dengan para profesional yang akan menangani laporanmu. Selama proses penyelesaian laporan, kamu akan didampingi oleh profesional dari lembaga.\n" +
                "\n"

        val title7 = "Apa yang saya dapatkan setelah melapor?\n"
        val desc7 ="Laporan kamu akan ditindaklanjuti oleh admin dari Dinas. Admin tersebut yang akan mengarahkan ke Lembaga yang dianggap mampu menangani kebutuhanmu. Lembaga-lembaga tersebut yang akan menangani ataupun mendampingi kamu sampai masalah kamu selesai.\n"

        val title8 = "Berapa lama laporan akan ditindaklanjuti?\n"
        val desc8 = "Tergantung kepada jenis kasus yang dilaporkan, keadaan penyintas kekerasan dan kebutuhan penyintas.\n"

        val title9 = "Bagaimana seharusnya melaporkan kasus yang saya alami?"
        val desc9 = "Bagaimana seharusnya melaporkan kasus yang saya alami?"

        val title10 = "Bagaimana proses Lapor?"
        val desc10 ="Setelah kamu mengirim laporanmu, Admin akan membaca kemudian menentukan Lembaga yang dapat membantu kamu. Respon dari Admin akan masuk di Kotak pesan kamu. Jika Laporan dianggap kurang atau Admin menganggap kamu butuh penjangkauan, maka Admin akan menghubungimu via telepon. \n" +
                "Setelah Lembaga ditunjuk, maka Lembaga akan mengarahkanmu untuk membuat janji bertemu. Status pesan Laporan dari menunggu, dalam proses, ataupun selesai, akan diubah oleh Admin dan muncul di status laporanmu.\n" +
                "Admin dan Lembaga akan mengikuti perkembangan Laporan kamu. Setelah Laporan diatasi, Lembaga akan memberitahu kepada Admin agar Admin dapat mengubah status laporanmu. \n"

        val title11 = "Bagaimana kerahasiaan laporan saya??"
        val desc11 = "Setiap data personal dan Laporan yang kamu masukan melalui aplikasi SETARA dijamin kerahasiaannya.\n"


        val title12 = "Bagaimana  jadwal pendampingan?"
        val desc12 = "Kamu dan lembaga akan menentukan bersama kapan pertemuan akan dilakukan."


        val title13 = "Kenapa saya harus menggunakan Setara?"
        val desc13= "Dengan menggunakan SETARA, kamu akan ditangani oleh profesional. Kami juga mengutamakan keamananmu, sehingga data diri dan isi laporan akan dijamin kerahasiaannya. SETARA ingin menjadi teman yang bisa membantumu menghadapi permasalahan yang sedang kamu hadapi.\n"


        val title14 = "Berapa biaya yang dibutuhkan selama proses pelaporan dan pendampingan?"
        val desc14 = "Tidak ada biaya yang perlu dikeluarkan untuk pelayanan dari Lembaga. Biaya pelaksanaan oleh Lembaga di bawah Dinas BPPM, telah dianggarkan dalam APBD daerah."


        val bundle1 = Bundle()
        bundle1.putString("title", title1)
        bundle1.putString("desc", desc1)

        val bundle2 = Bundle()
        bundle2.putString("title", title2)
        bundle2.putString("desc", desc2)

        val bundle3 = Bundle()
        bundle3.putString("title", title3)
        bundle3.putString("desc", desc3)

        val bundle4 = Bundle()
        bundle4.putString("title", title4)
        bundle4.putString("desc", desc4)

        val bundle5 = Bundle()
        bundle5.putString("title", title5)
        bundle5.putString("desc", desc5)

        val bundle6 = Bundle()
        bundle6.putString("title", title6)
        bundle6.putString("desc", desc6)

        val bundle7 = Bundle()
        bundle7.putString("title", title7)
        bundle7.putString("desc", desc7)

        val bundle8 = Bundle()
        bundle8.putString("title", title8)
        bundle8.putString("desc", desc8)

        val bundle9 = Bundle()
        bundle9.putString("title", title9)
        bundle9.putString("desc", desc9)

        val bundle10 = Bundle()
        bundle10.putString("title", title10)
        bundle10.putString("desc", desc10)

        val bundle11 = Bundle()
        bundle11.putString("title", title11)
        bundle11.putString("desc", desc11)

        val bundle12 = Bundle()
        bundle12.putString("title", title12)
        bundle12.putString("desc", desc12)

        val bundle13 = Bundle()
        bundle13.putString("title", title13)
        bundle13.putString("desc", desc13)

        val bundle14 = Bundle()
        bundle14.putString("title", title14)
        bundle14.putString("desc", desc14)

        view.btnHubungiKami.setOnClickListener {
            startActivity(Intent(this.activity, BantuanContactUsActivity::class.java))
        }

        view.cvFAQ1.setOnClickListener {
            val intent = Intent(this.activity, BantuanFAQ::class.java)
            intent.putExtras(bundle1)
            startActivity(intent)
        }

        view.cvFAQ2.setOnClickListener {
            val intent = Intent(this.activity, BantuanFAQ::class.java)
            intent.putExtras(bundle2)
            startActivity(intent)
        }


        view.cvFAQ3.setOnClickListener {
            val intent = Intent(this.activity, BantuanFAQ::class.java)
            intent.putExtras(bundle3)
            startActivity(intent)
        }

        view.cvFAQ4.setOnClickListener {
            val intent = Intent(this.activity, BantuanFAQ::class.java)
            intent.putExtras(bundle4)
            startActivity(intent)
        }


        view.cvFAQ5.setOnClickListener {
            val intent = Intent(this.activity, BantuanFAQ::class.java)
            intent.putExtras(bundle5)
            startActivity(intent)
        }

        view.cvFAQ6.setOnClickListener {
            val intent = Intent(this.activity, BantuanFAQ::class.java)
            intent.putExtras(bundle6)
            startActivity(intent)
        }


        view.cvFAQ7.setOnClickListener {
            val intent = Intent(this.activity, BantuanFAQ::class.java)
            intent.putExtras(bundle7)
            startActivity(intent)
        }

        view.cvFAQ8.setOnClickListener {
            val intent = Intent(this.activity, BantuanFAQ::class.java)
            intent.putExtras(bundle8)
            startActivity(intent)
        }


        view.cvFAQ9.setOnClickListener {
            val intent = Intent(this.activity, BantuanFAQ::class.java)
            intent.putExtras(bundle9)
            startActivity(intent)
        }

        view.cvFAQ10.setOnClickListener {
            val intent = Intent(this.activity, BantuanFAQ::class.java)
            intent.putExtras(bundle10)
            startActivity(intent)
        }


        view.cvFAQ11.setOnClickListener {
            val intent = Intent(this.activity, BantuanFAQ::class.java)
            intent.putExtras(bundle11)
            startActivity(intent)
        }

        view.cvFAQ12.setOnClickListener {
            val intent = Intent(this.activity, BantuanFAQ::class.java)
            intent.putExtras(bundle12)
            startActivity(intent)
        }


        view.cvFAQ13.setOnClickListener {
            val intent = Intent(this.activity, BantuanFAQ::class.java)
            intent.putExtras(bundle13)
            startActivity(intent)
        }

        view.cvFAQ14.setOnClickListener {
            val intent = Intent(this.activity, BantuanFAQ::class.java)
            intent.putExtras(bundle14)
            startActivity(intent)
        }


        return view
    }
}
