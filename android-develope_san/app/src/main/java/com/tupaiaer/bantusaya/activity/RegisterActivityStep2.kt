package com.tupaiaer.bantusaya.activity

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.tupaiaer.bantusaya.Constants.PICK_PHOTO_REQUEST
import com.tupaiaer.bantusaya.R
import kotlinx.android.synthetic.main.activity_register_step1.*
import kotlinx.android.synthetic.main.activity_register_step2.*
import kotlinx.android.synthetic.main.activity_report_vic.*
import java.util.*
import android.widget.DatePicker
import android.app.AlertDialog.THEME_TRADITIONAL





class RegisterActivityStep2 : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    var gender: String = ""
    var domisili = arrayOf("Kota Yogyakarta", "Kab. Sleman", "Kab. Bantul", "Kab. Gunung Kidul", "Kab. Kulon Progo")
    var domisiliSelected: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_step2)

        toolbarRegStep2.setNavigationIcon(R.drawable.ic_arrow)
        toolbarRegStep2.setNavigationOnClickListener {
            onBackPressed()
        }

        getDate()

        val noHP:String = intent.getStringExtra("data1")
        val email:String = intent.getStringExtra("data2")
        val password:String = intent.getStringExtra("data3")

        ibLakiAcc.setOnClickListener {
            ibLakiAcc.setBackgroundResource(R.drawable.btn_border)
            ibPerempuanAcc.setBackgroundResource(R.color.btn_gender_color)
            gender = "Laki-Laki"
            warnGender.visibility = View.GONE
        }

        ibPerempuanAcc.setOnClickListener {
            ibPerempuanAcc.setBackgroundResource(R.drawable.btn_border)
            ibLakiAcc.setBackgroundResource(R.color.btn_gender_color)
            gender = "Perempuan"
            warnGender.visibility = View.GONE
        }

        spinDomisiliAcc!!.setOnItemSelectedListener(this)

        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, domisili)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinDomisiliAcc!!.setAdapter(aa)

        btnLanjut2.setOnClickListener {

            if (etNamaLengkapAcc.text.isNullOrBlank() || gender.isNullOrBlank() || etTglLahirAcc.text.isNullOrBlank()) {

                if (etNamaLengkapAcc.text.isNullOrBlank()) {
                    etNamaLengkapAcc.setError("Nama lengkap harus diisi")
                }

                if (gender.isNullOrBlank()) {
                    warnGender.visibility = View.VISIBLE
                } else {
                    warnGender.visibility = View.GONE
                }

                if (etTglLahirAcc.text.isNullOrBlank()) {
                    etTglLahirAcc.setError("Tanggal lahir harus diisi")
                }
            } else {
                val bundle = Bundle()
                bundle.putString("data1", noHP)
                bundle.putString("data2", email)
                bundle.putString("data3", password)
                bundle.putString("data4", etNamaLengkapAcc.text.toString())
                bundle.putString("data5", gender)
                bundle.putString("data6", etTglLahirAcc.text.toString())
                bundle.putString("data7", domisiliSelected)

                val intent = Intent(this, RegisterActivityStep3::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }
    }

    fun getDate() {
        val c = Calendar.getInstance()
        var y = c.get(Calendar.YEAR)
        var m = c.get(Calendar.MONTH)
        var d = c.get(Calendar.DAY_OF_MONTH)

        ibCalendar.setOnClickListener {
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                d = dayOfMonth
                m = monthOfYear + 1
                y = year
                etTglLahirAcc.setText("$dayOfMonth-$monthOfYear-$year")
            }, d, m, y)
            dpd.show()
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        Log.v("Domisili", domisili[position])
        domisiliSelected = domisili[position]
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }
}
