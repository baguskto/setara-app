package com.tupaiaer.bantusaya.utils

import com.google.gson.Gson

fun Any.toJson() = Gson().toJsonTree(this)