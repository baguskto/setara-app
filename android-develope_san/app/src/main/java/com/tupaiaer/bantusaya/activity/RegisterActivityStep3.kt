package com.tupaiaer.bantusaya.activity

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.util.Log
import android.widget.ImageView
import com.tupaiaer.bantusaya.BantuSayaApp
import com.tupaiaer.bantusaya.Constants
import com.tupaiaer.bantusaya.Constants.PICK_PHOTO_REQUEST
import com.tupaiaer.bantusaya.Constants.TAKE_PHOTO_REQUEST
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.model.User
import com.tupaiaer.bantusaya.model.UserRegister
import com.tupaiaer.bantusaya.utils.toJson
import com.tupaiaer.bantusaya.utils.toast
import kotlinx.android.synthetic.main.activity_register_step1.*
import kotlinx.android.synthetic.main.activity_register_step3.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivityStep3 : AppCompatActivity() {

    var fileUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_step3)

        toolbarRegStep3.setNavigationIcon(R.drawable.ic_arrow)
        toolbarRegStep3.setNavigationOnClickListener {
            onBackPressed()
        }

        setupView()
    }

    private fun setupView() {


        val username = intent.getStringExtra("data4").toString()
        val email = intent.getStringExtra("data2").toString()
        val password = intent.getStringExtra("data3").toString()
        val passwordConf = password
        val ages = "20"
        val gender = intent.getStringExtra("data5").toString()
        val phone = intent.getStringExtra("data1").toString()
        val address = intent.getStringExtra("data7").toString()
        val birthDate = intent.getStringExtra("data6").toString()
        val userPrev = "1"

        Log.v("username", username)
        Log.v("email", email)
        Log.v("password", password)
        Log.v("gender", gender)
        Log.v("phone", phone)
        Log.v("address", address)
        Log.v("birthDate", birthDate)


        ibKtpAcc.setOnClickListener {
            pickPhotoFromGallery()
        }

        btnLanjut3.setOnClickListener {
            saveUser()
        }
    }

    private fun saveUser() {

        val username = intent.getStringExtra("data4").toString()
        val email = intent.getStringExtra("data2").toString()
        val password = intent.getStringExtra("data3").toString()
        val gender = intent.getStringExtra("data5").toString()
        val phone = intent.getStringExtra("data1").toString()
        val address = intent.getStringExtra("data7").toString()
        val birthDate = intent.getStringExtra("data6").toString()
        val userPrev = "1"


        val user = UserRegister(
            username = username,
            email = email,
            password = password,
            passwordConfirmation = password,
            gender = gender,
            phoneNumber = phone,
            address = address,
            birthDate = birthDate,
            userPrev = userPrev,
            profilPic = null,
            identityPic = null,
            domisili = address,
            identityNum = etNikAcc.text.toString()
        )

        BantuSayaApp.apiService
            .saveUser(user)
            .enqueue(object : Callback<Any> {
                override fun onFailure(call: Call<Any>, t: Throwable) {
                }

                override fun onResponse(call: Call<Any>, response: Response<Any>) {
                    Snackbar.make(rootRegisterStep3, "Berhasil mendaftar!", Snackbar.LENGTH_SHORT).show()
                    val intent = Intent(this@RegisterActivityStep3, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                    Log.d("MAIN", "response : ${response.body()?.toJson()}")
                }

            })
    }

    private fun pickPhotoFromGallery() {
        val pickImageIntent = Intent(Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        startActivityForResult(pickImageIntent, Constants.PICK_PHOTO_REQUEST)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int,
                                  data: Intent?) {
        if (resultCode == Activity.RESULT_OK
            && requestCode == TAKE_PHOTO_REQUEST) {
            //photo from camera
            //display the photo on the imageview
//            imageView.setImageURI(fileUri)
        }else if(resultCode == Activity.RESULT_OK
            && requestCode == PICK_PHOTO_REQUEST){
            //photo from gallery
            fileUri = data?.data
            ibKtpAcc.setImageURI(fileUri)
            ibKtpAcc.scaleType = ImageView.ScaleType.CENTER_CROP
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}
