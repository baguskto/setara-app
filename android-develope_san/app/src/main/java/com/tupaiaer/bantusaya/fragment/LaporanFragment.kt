package com.tupaiaer.bantusaya.fragment


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tupaiaer.bantusaya.BantuSayaApp
import com.tupaiaer.bantusaya.PrefManager

import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.activity.ReportationReplyActivity
import com.tupaiaer.bantusaya.activity.ReportationViewRespondActivity
import com.tupaiaer.bantusaya.adapter.ReportAdapter
import com.tupaiaer.bantusaya.model.ProfileResponse
import com.tupaiaer.bantusaya.model.Report
import com.tupaiaer.bantusaya.model.ReportResponse
import com.tupaiaer.bantusaya.utils.toast
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.fragment_laporan.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class LaporanFragment : Fragment() {

    private val reportList = mutableListOf<Report>()
    private val reportAdapter = ReportAdapter(reportList, this::onItemClick)
    private lateinit var pref: PrefManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_laporan, container, false)

        pref = PrefManager(activity!!)

        if (pref.blankReport == "null" || pref.blankReport.isNullOrBlank()) {
            view.ivBlankReport.visibility = View.VISIBLE
            view.tvBlankReport.visibility = View.VISIBLE
            view.rvAllReport.visibility = View.GONE
        }
        else {
            view.rvAllReport.visibility = View.VISIBLE
            view.ivBlankReport.visibility = View.GONE
            view.tvBlankReport.visibility = View.GONE
        }


        return view
    }

    companion object {
        fun newInstance(): LaporanFragment {
            val fragment = LaporanFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
    }

    private fun setupView() = view?.apply {

        rvAllReport.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = reportAdapter
        }
        getAllReport()
    }

    private fun onItemClick(report: Report) {
        val bundle = Bundle()
        bundle.putString("data1", report.id)
        bundle.putString("data2", report.title)
        bundle.putString("data3", report.description)
        bundle.putString("data4", report.status)
        bundle.putString("data5", report.created_at)
        bundle.putString("data6", report.statusPelapor)
        bundle.putString("data7", report.suspect)
        bundle.putString("data8", report.crimeScene)
        bundle.putString("data9", report.victimName)
        bundle.putString("data10", report.victimGender)
        bundle.putString("data11", report.victimAges)
        bundle.putString("data12", report.victimAddress)

        val intent = Intent(this.activity, ReportationViewRespondActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private fun getAllReport() {
        BantuSayaApp.apiService.allReport()
            .enqueue(object : Callback<ReportResponse<List<Report>>> {
                override fun onFailure(call: Call<ReportResponse<List<Report>>>, t: Throwable) {
                }

                override fun onResponse(call: Call<ReportResponse<List<Report>>>, response: Response<ReportResponse<List<Report>>>) {
                    val reports: List<Report>? = response.body()?.laporans
                    pref.blankReport = reports.toString()

                    Log.v("report", pref.blankReport)
                        onResponseSuccess(reports ?: emptyList())
                    }
            })
    }

    private fun onResponseSuccess(students: List<Report>) {
        reportList.clear()
        reportList.addAll(students)
        reportAdapter.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        getAllReport()
    }
}
