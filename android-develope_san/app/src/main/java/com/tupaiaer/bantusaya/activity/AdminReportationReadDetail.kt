package com.tupaiaer.bantusaya.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.tupaiaer.bantusaya.R
import kotlinx.android.synthetic.main.activity_admin_reportation_read_detail.*

class AdminReportationReadDetail : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_reportation_read_detail)

        tbDetailReportAdmin3.setNavigationIcon(R.drawable.ic_arrow)
        tbDetailReportAdmin3.setNavigationOnClickListener {
            onBackPressed()
        }

        val data1 = intent.getStringExtra("data1")
        val data2 = intent.getStringExtra("data2")
        val data3 = intent.getStringExtra("data3")
        val data4 = intent.getStringExtra("data4")
        val data5 = intent.getStringExtra("data5")
        val data6 = intent.getStringExtra("data6")
        val data7 = intent.getStringExtra("data7")
        val data8 = intent.getStringExtra("data8")
        val data9 = intent.getStringExtra("data9")
        val data10 = intent.getStringExtra("data10")
        val data11 = intent.getStringExtra("data11")
        val data12 = intent.getStringExtra("data12")
        val data13 = intent.getStringExtra("data13")
        val data14 = intent.getStringExtra("data14")

        tvAdminIdReportDetail.text = data1
        tvAdminTitleDetail.text = data2
        tvAdminCreatedDetail.text = data5
        tvAdminStatusPelaporDetail.text = data6
        tvAdminNamaPelaporDetail.text = data7
        tvAdminNamaKorbanDetail.text = data9
        tvAdminJenisKelaminDetail.text = data10
        tvAdminUmurKorbanDetail.text = data11
        tvAdminNamaPelakuDetail.text = data7
        tvAdminDeskripsiKejadianDetail.text = data3
        tvAdminLokasiKejadianDetail.text = data8
    }
}
