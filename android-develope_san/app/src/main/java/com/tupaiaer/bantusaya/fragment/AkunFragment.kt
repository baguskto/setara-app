package com.tupaiaer.bantusaya.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tupaiaer.bantusaya.BantuSayaApp
import com.tupaiaer.bantusaya.PrefManager

import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.activity.BantuanContactUsActivity
import com.tupaiaer.bantusaya.activity.MainActivity
import com.tupaiaer.bantusaya.activity.ProfileChangePass
import com.tupaiaer.bantusaya.activity.ProfileEditActivity
import com.tupaiaer.bantusaya.model.ProfileResponse
import kotlinx.android.synthetic.main.fragment_akun.*
import kotlinx.android.synthetic.main.fragment_akun.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.content.DialogInterface
import android.os.Build
import android.support.v7.app.AlertDialog
import android.app.AlarmManager
import android.content.Context.ALARM_SERVICE
import android.support.v4.content.ContextCompat.getSystemService
import android.app.PendingIntent
import android.content.Context



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AkunFragment : Fragment() {

    private lateinit var pref: PrefManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_akun, container, false)

        getUser()
        pref = PrefManager(activity!!)

        view.cvSuntingProfil.setOnClickListener {
            startActivity(Intent(this.activity, ProfileEditActivity::class.java))
        }
        view.cvKataSandi.setOnClickListener {
            startActivity(Intent(this.activity, ProfileChangePass::class.java))
        }
        view.cvPengelola.setOnClickListener {
            startActivity(Intent(this.activity, BantuanContactUsActivity::class.java))
        }

        if (pref.name.isNullOrBlank()) {
            view.logout.visibility = View.GONE
        } else {
            view.logout.visibility = View.VISIBLE
        }

        view.logout.setOnClickListener {
            val builder: AlertDialog.Builder
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = AlertDialog.Builder(activity!!, android.R.style.Theme_Material_Dialog_Alert)
            } else {
                builder = AlertDialog.Builder(activity!!)
            }
            builder
                .setTitle("Keluar")
                .setMessage("Anda yakin ingin keluar?")
                .setPositiveButton("Ya") { dialog, which ->
                    pref.logOut()
                    val mStartActivity = Intent(this.activity, MainActivity::class.java)
                    val mPendingIntentId = 123456
                    val mPendingIntent = PendingIntent.getActivity(
                        this.activity, mPendingIntentId, mStartActivity,
                        PendingIntent.FLAG_CANCEL_CURRENT
                    )
                    val mgr = this.activity!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                    mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent)
                    System.exit(0)
                }
                .setNegativeButton("Tidak") { dialog, which ->
                }
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show()
        }


        return view
    }

    private fun getUser() {
        BantuSayaApp.apiService.userProfile()
            .enqueue(object : Callback<ProfileResponse> {
                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                }

                override fun onResponse(call: Call<ProfileResponse>, response: Response<ProfileResponse>) {
                    val name = response.body()?.user?.username
                    if (name == null) {
                        tvNameProfile.text = ""
                        ivProfile.visibility = View.GONE
                        tvAkun.visibility = View.GONE
                        cvSuntingProfil.visibility = View.GONE
                        cvKataSandi.visibility = View.GONE
                    } else {
                        tvNameProfile.text = name.toString()
                        ivProfile.visibility = View.VISIBLE
//                    Log.v("username", response.body()?.user?.username)
                    }
                }
            })
    }
}
