package com.tupaiaer.bantusaya.model

import com.google.gson.annotations.SerializedName


data class ConversationResponse(
    val result: Boolean,
    val replies: List<Reply>
)

data class Reply(
    val conversation_id: Int,
    val created_at: String,
    val id: Int,
    val institution_id: Any,
    val reply_body: String,
    val updated_at: String,
    val user_id: User
)

data class SendReply(
    val conv_id: Int,
    val reply_body: String
)
