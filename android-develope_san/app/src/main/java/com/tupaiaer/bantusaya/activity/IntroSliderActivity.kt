package com.tupaiaer.bantusaya.activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.annotation.ColorInt
import android.support.v4.app.Fragment
import android.widget.TextView
import android.widget.Toast
import com.github.paolorotolo.appintro.AppIntro
import com.github.paolorotolo.appintro.AppIntroFragment
import com.tupaiaer.bantusaya.PrefManager
import com.tupaiaer.bantusaya.R
import kotlinx.android.synthetic.main.fragment_intro_content.*
import kotlinx.android.synthetic.main.intro_layout.*

class IntroSliderActivity : AppIntro() {

    private lateinit var pref: PrefManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        pref = PrefManager(this)
        if (!pref.isFirstTimeLaunch()) {

            if (pref.name == "adminsetara") {
                finish()
                startActivity(Intent(this, AdminReportationStatusActivity::class.java))
            } else {
                launchHomeScreen()
            }
        }


        addSlide(
            AppIntroFragment.newInstance(
                "Lapor", "Anda dapat melaporkan kejadian yang anda alami atau yang anda lihat",
                R.drawable.onboarding1, Color.parseColor("white"), Color.parseColor("black"), Color.parseColor("black")
            )
        )
        addSlide(
            AppIntroFragment.newInstance(
                "Psiko - Edukasi",
                "Dapatkan berbagai informasi lengkap dan terpercaya seputar pemberdayaan perempuan dan anak.",
                R.drawable.onboarding2,
                Color.parseColor("white"),
                Color.parseColor("black"),
                Color.parseColor("black")
            )
        )
        addSlide(
            AppIntroFragment.newInstance(
                "Konsultasikan dengan ahlinya",
                "Cerita yuk ke konsultan psikologi dan hukum kami yang dengan senang hati setia mendengarkan",
                R.drawable.onboarding3,
                Color.parseColor("white"),
                Color.parseColor("black"),
                Color.parseColor("black")
            )
        )

        setBarColor(Color.parseColor("#FFFFFF"))
        setIndicatorColor(R.color.colorPrimary, R.color.white)

        skip.text = "Lewati"
        done.text = "Mulai"
    }

    private fun launchHomeScreen() {
        pref.setFirstTimeLaunch(false)
        finish()
        startActivity(Intent(this@IntroSliderActivity, MainActivity::class.java))
        finish()
    }


    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        launchHomeScreen()
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        launchHomeScreen()
    }

    override fun onSlideChanged(oldFragment: Fragment?, newFragment: Fragment?) {
        super.onSlideChanged(oldFragment, newFragment)
        // Do something when the slide changes.

    }
}