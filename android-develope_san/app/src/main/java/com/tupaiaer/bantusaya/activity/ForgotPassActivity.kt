package com.tupaiaer.bantusaya.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.tupaiaer.bantusaya.R
import kotlinx.android.synthetic.main.activity_forgot_pass.*

class ForgotPassActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_pass)

        toolbarForgetPass.setNavigationIcon(R.drawable.ic_arrow_back_black)
        toolbarForgetPass.setNavigationOnClickListener {
            onBackPressed()
        }
    }
}
