package com.tupaiaer.bantusaya.activity

import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import com.tupaiaer.bantusaya.BantuSayaApp
import com.tupaiaer.bantusaya.ConnectionReceiver
import com.tupaiaer.bantusaya.PrefManager
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.fragment.AkunFragment
import com.tupaiaer.bantusaya.fragment.BantuanFragment
import com.tupaiaer.bantusaya.fragment.HomeFragment
import com.tupaiaer.bantusaya.fragment.LaporanFragment
import com.tupaiaer.bantusaya.model.ProfileResponse
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), ConnectionReceiver.ConnectionReceiverListener {

    private lateinit var pref: PrefManager

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.home -> {
                val fragment = HomeFragment.newInstance()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.bantuan -> {
                val fragment = BantuanFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.laporan -> {
                val fragment = LaporanFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.akun -> {
                val fragment = AkunFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun addFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out)
            .replace(R.id.content, fragment, fragment.javaClass.getSimpleName())
            .commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        baseContext.registerReceiver(ConnectionReceiver(), IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        BantuSayaApp.instance.setConnectionListener(this)
        pref = PrefManager(this)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        val fragment = HomeFragment.newInstance()
        addFragment(fragment)

        btnSettings.setOnClickListener {
            startActivity(Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS))
        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (isConnected) {
            linearConnection.visibility = View.GONE
        }
        else {
            linearConnection.visibility = View.VISIBLE
        }
    }

    private var doubleBackToExitPressedOnce = false

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            this.finish()
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Ketuk lagi untuk keluar", Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }
}