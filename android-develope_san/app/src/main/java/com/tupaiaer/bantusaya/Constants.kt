package com.tupaiaer.bantusaya

/**
 * Created by @ilhamsuaib on 07/12/18.
 * github.com/ilhamsuaib
 */

object Constants {
    //shared prefenrence
    const val SP_PREF_NAME = "bantu_saya.sp"
    const val SP_HAS_INTRO_SLIDER = "sp.has_intro_slider"
    const val SP_TOKEN = "sp.token"
    const val SP_HAS_LOGIN = "sp.has_login"
    const val STATUS_PELAPOR = "status_pelapor"
    const val KATEGORI_LAPOR = "kategori"
    const val SP_ID_USER = "id"
    const val SP_NAME = "name"
    const val SP_BIRTHDATE = "birthdate"
    const val SP_ADDRESS = "address"
    const val SP_EMAIL = "email"
    const val SP_PHONE = "phone"
    const val SP_ID_CONV = "id_conv"
    const val SP_USER_ID_CONV = "user_id"
    const val SP_INS_ID_CONV = "ins_id"
    const val SP_REPLY_BODY = "reply_body"
    const val SP_CREATED_CONV = "created_conv"
    const val BLANK_REPORT = "blank_report"
    val TAKE_PHOTO_REQUEST: Int = 2
    val PICK_PHOTO_REQUEST: Int = 1

}