package com.tupaiaer.bantusaya.activity

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.Places
import com.google.android.gms.location.places.ui.PlacePicker
import com.tupaiaer.bantusaya.BantuSayaApp
import com.tupaiaer.bantusaya.Constants
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.model.Report
import com.tupaiaer.bantusaya.utils.toJson
import com.tupaiaer.bantusaya.utils.toast
import kotlinx.android.synthetic.main.activity_register_step3.*
import kotlinx.android.synthetic.main.activity_report_vic.*
import kotlinx.android.synthetic.main.activity_report_wit.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReportWit : AppCompatActivity(), AdapterView.OnItemSelectedListener, GoogleApiClient.OnConnectionFailedListener {

    var gender = ""
    var relationship = arrayOf("Kota Yogyakarta", "Kab. Sleman", "Kab. Bantul", "Kab. Gunung Kidul", "Kab. Kulon Progo")
    var relationshipSelected: String = ""

    private lateinit var mGoogleApiClient: GoogleApiClient
    private var PLACE_PICKER_REQUEST = 1
    private var latLng = ""
    var fileUri: Uri? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_wit)

        toolbarReportWit.setNavigationIcon(R.drawable.ic_arrow)
        toolbarReportWit.setNavigationOnClickListener {
            onBackPressed()
        }

        setupView()
    }

    private fun setupView() {

        spinRelationship!!.setOnItemSelectedListener(this)

        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, relationship)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinRelationship!!.setAdapter(aa)

        ibLakiWit.setOnClickListener {
            ibLakiWit.setBackgroundResource(R.drawable.btn_border)
            ibPerempuanWit.setBackgroundResource(R.color.btn_gender_color)
            gender = "Laki-Laki"
        }

        ibPerempuanWit.setOnClickListener {
            ibPerempuanWit.setBackgroundResource(R.drawable.btn_border)
            ibLakiWit.setBackgroundResource(R.color.btn_gender_color)
            gender = "Perempuan"
        }

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addApi(Places.GEO_DATA_API)
            .addApi(Places.PLACE_DETECTION_API)
            .enableAutoManage(this, this)
            .build()

        ivMapWit.setOnClickListener {
            val PLACE_PICKER_REQUEST = 1
            val builder = PlacePicker.IntentBuilder()

            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST)
        }

        btnSaveReportWit.setOnClickListener {

            if (etNamaKorbanWit.text.isNullOrBlank() || etUmurKorbanWit.text.isNullOrBlank() || gender.isNullOrBlank() || etDesKejadianWit.text.isNullOrBlank() || latLng.isNullOrBlank() ) {
                Snackbar.make(
                    rootReportWit, // Parent view
                    "Semua data harus diisi!", // Message to show
                    Snackbar.LENGTH_SHORT // How long to display the message.
                ).show()            } else {
                saveReportWit()
                startActivity(Intent(this, ReportResult::class.java))
                finish()
            }
        }

        ivBuktiWit.setOnClickListener {
            pickPhotoFromGallery()
        }
    }

    private fun saveReportWit() {

        val statusPelapor:String = intent.getStringExtra(Constants.STATUS_PELAPOR)
        val kategori:String = intent.getStringExtra(Constants.KATEGORI_LAPOR)
        val status = "pending"

        val report = Report(
            id = null,
            title = kategori,
            description = etDesKejadianWit.text.toString(),
            status = status,
            statusPelapor = statusPelapor,
            suspect = null,
            crimeScene = null,
            victimName = etNamaKorbanWit.text.toString(),
            victimGender = gender,
            victimAddress = latLng,
            victimAges = etUmurKorbanWit.text.toString(),
            created_at = null
        )

        BantuSayaApp.apiService
            .saveReport(report)
            .enqueue(object : Callback<Any> {
                override fun onFailure(call: Call<Any>, t: Throwable) {
                }

                override fun onResponse(call: Call<Any>, response: Response<Any>) {
                    Snackbar.make(
                        rootReportWit, // Parent view
                        "Laporan berhasil dikirim!", // Message to show
                        Snackbar.LENGTH_SHORT // How long to display the message.
                    ).show()
                    Log.d("MAIN", "response : ${response.body()?.toJson()}")
                }

            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                var place: Place
                place = PlacePicker.getPlace(data, this)
                var stBuilder = StringBuilder()
                var placeName = String.format("%s", place.getName())
                stBuilder.append(placeName)
                ivMapWit.visibility = View.GONE
                tvMapWit.visibility = View.VISIBLE
                tvMapWit.text = stBuilder.toString()
                latLng = placeName
            }
        }

//        if (resultCode == Activity.RESULT_OK
//            && requestCode == Constants.TAKE_PHOTO_REQUEST
//        ) {
//        }else if(resultCode == Activity.RESULT_OK
//            && requestCode == Constants.PICK_PHOTO_REQUEST
//        ){
//            fileUri = data?.data
//            ivBuktiWit.setImageURI(fileUri)
//            ivBuktiWit.scaleType = ImageView.ScaleType.CENTER_CROP
//        } else {
//            super.onActivityResult(requestCode, resultCode, data)
//        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    override fun onStart() {
        super.onStart()
        mGoogleApiClient.connect()
    }

    override fun onStop() {
        mGoogleApiClient.disconnect()
        super.onStop()
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        Log.v("Relationship", relationship[position])
        relationshipSelected = relationship[position]
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    private fun pickPhotoFromGallery() {
        val pickImageIntent = Intent(Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        startActivityForResult(pickImageIntent, Constants.PICK_PHOTO_REQUEST)
    }

}
