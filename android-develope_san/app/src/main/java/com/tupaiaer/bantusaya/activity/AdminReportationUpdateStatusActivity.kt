package com.tupaiaer.bantusaya.activity

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.tupaiaer.bantusaya.BantuSayaApp
import com.tupaiaer.bantusaya.PrefManager
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.adapter.ConvAdapter
import com.tupaiaer.bantusaya.model.ConversationResponse
import com.tupaiaer.bantusaya.model.Laporan
import com.tupaiaer.bantusaya.model.Reply
import com.tupaiaer.bantusaya.model.UpdateStatusResponse
import com.tupaiaer.bantusaya.utils.sendRequest
import kotlinx.android.synthetic.main.activity_admin_reportation_update_status.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AdminReportationUpdateStatusActivity : AppCompatActivity() {

    private var statusPelapor = ""
    private val conversationList = mutableListOf<Reply>()
    private val conversationAdapter = ConvAdapter(conversationList, this::onItemClick)
    private lateinit var pref: PrefManager

    var param: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_reportation_update_status)

        rvAdminConversation.layoutManager = LinearLayoutManager(this)

        setupView()
    }


    private fun setupView() {

        pref = PrefManager(this)

        val data1 = intent.getStringExtra("data1")
        val data2 = intent.getStringExtra("data2")
        val data3 = intent.getStringExtra("data3")
        val data4 = intent.getStringExtra("data4")
        val data5 = intent.getStringExtra("data5")
        val data6 = intent.getStringExtra("data6")
        val data7 = intent.getStringExtra("data7")
        val data8 = intent.getStringExtra("data8")
        val data9 = intent.getStringExtra("data9")
        val data10 = intent.getStringExtra("data10")
        val data11 = intent.getStringExtra("data11")
        val data12 = intent.getStringExtra("data12")
        val data13 = intent.getStringExtra("data13")
        val data14 = intent.getStringExtra("data14")

        val bundle = Bundle()
        bundle.putString("data1", data1)
        bundle.putString("data2", data2)
        bundle.putString("data3", data3)
        bundle.putString("data4", data4)
        bundle.putString("data5", data5)
        bundle.putString("data6", data6)
        bundle.putString("data7", data7)
        bundle.putString("data8", data8)
        bundle.putString("data9", data9)
        bundle.putString("data10", data10)
        bundle.putString("data11", data11)
        bundle.putString("data12", data12)
        bundle.putString("data13", data13)
        bundle.putString("data14", data14)

        param = data1

        tvReportIdAdminUpdate.text = "LAPORAN ID-" + data1
        tvReportId2AdminUpdate.text = "LAPORAN ID-" + data1
        tvTitleAdminUpdate.text = data2
        tvDateAdminUpdate.text = data6

        cvDetailReportAdminUpdate.setOnClickListener {
            val intent = Intent(this, AdminReportationReadDetail::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }

        Log.v("status", data4.toString())

        if (data4 == "pending" || data4 == "Pending") {
            btnPending.setBackgroundResource(R.color.colorPrimary)
            btnPending.setTextColor(ContextCompat.getColorStateList(this, R.color.white))

            if (data4 == "progress" || data4 == "Progress") {
                btnProgress.setBackgroundResource(R.color.colorPrimary)
                btnProgress.setTextColor(ContextCompat.getColorStateList(this, R.color.white))
            }

            if (data4 == "solved" || data4 == "Solved") {
                btnSolved.setBackgroundResource(R.color.colorPrimary)
                btnProgress.setTextColor(ContextCompat.getColorStateList(this, R.color.white))
            }

            btnPending.setOnClickListener {
                btnPending.setBackgroundResource(R.color.colorPrimary)
                btnPending.setTextColor(ContextCompat.getColorStateList(this, R.color.white))
                statusPelapor = "pending"
            }

            btnProgress.setOnClickListener {
                btnProgress.setBackgroundResource(R.color.colorPrimary)
                btnProgress.setTextColor(ContextCompat.getColorStateList(this, R.color.white))
                statusPelapor = "progress"
            }

            btnSolved.setOnClickListener {
                btnSolved.setBackgroundResource(R.color.colorPrimary)
                btnSolved.setTextColor(ContextCompat.getColorStateList(this, R.color.white))
                statusPelapor = "solved"
            }
        }

        btnWriteConversationAdmin.setOnClickListener {
            val intent = Intent(this, ReportationWriteReplyActivity::class.java)
            startActivity(intent)
        }

        rvAdminConversation.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = conversationAdapter
        }

        getAllConversationAdmin(data1)

    }

    private fun getAllConversationAdmin(param: String) {
        BantuSayaApp.apiService.getConversation(param)
            .enqueue(object : Callback<ConversationResponse> {
                override fun onFailure(call: Call<ConversationResponse>, t: Throwable) {
                    Log.v("error", t.localizedMessage)
                }

                override fun onResponse(call: Call<ConversationResponse>, response: Response<ConversationResponse>) {
                    val reports: List<Reply>? = response.body()?.replies
                    pref.blankReport = reports.toString()

                    Log.v("reports", pref.blankReport)

                    onResponseSuccess(reports ?: emptyList())
//                    rvConversation.adapter = conversationAdapter
                }
            })
    }

        private fun onItemClick(reply: Reply) {
            val bundle = Bundle()

            pref.idConversation = "${reply.conversation_id}"
            pref.idUserConversation = "${reply.user_id}"
            pref.idInstutionConversation = "${reply.institution_id}"
            pref.replyBody = "${reply.reply_body}"
            pref.createdConversation = "${reply.created_at}"
            pref.name = "${reply.user_id.username}"

            val intent = Intent(this, ReportationReplyActivity::class.java)
            startActivity(intent)
        }

        private fun onResponseSuccess(students: List<Reply>) {
            conversationList.clear()
            conversationList.addAll(students)
            conversationAdapter.notifyDataSetChanged()
        }

        private fun getAllConversation(statusPelapor: String) {
            BantuSayaApp.apiService
                .updateStatus(statusPelapor)
//            .sendRequest(this::onErrorUpdate, this::onSuccessUpdate)
        }

        private fun onSuccessUpdate() {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        private fun onErrorUpdate() {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    }