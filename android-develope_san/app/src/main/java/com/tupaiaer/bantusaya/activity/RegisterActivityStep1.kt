package com.tupaiaer.bantusaya.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.tupaiaer.bantusaya.R
import kotlinx.android.synthetic.main.activity_register_step1.*
import java.util.regex.Pattern


class RegisterActivityStep1 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_step1)

        toolbarRegStep1.setNavigationIcon(R.drawable.ic_arrow)
        toolbarRegStep1.setNavigationOnClickListener {
            onBackPressed()
        }



        btnLanjut.setOnClickListener {

            val getEmailId = etEmailAcc.text.toString()

            val size = etPasswordAcc.length()

            if (etNoHpAcc.text.isNullOrBlank() || etEmailAcc.text.isNullOrBlank() || etPasswordAcc.text.isNullOrBlank() || size < 8 || !isEmailValid(getEmailId)) {

                if (size < 8) {
                    etPasswordAcc.setError("Masukan minimum 8 karakter")
                }

                if (etNoHpAcc.text.isNullOrBlank()) {
                    etNoHpAcc.setError("Nomor handphone harus diisi")
                }

                if (etEmailAcc.text.isNullOrBlank()) {
                    etEmailAcc.setError("Email harus diisi")
                }

                if (etPasswordAcc.text.isNullOrBlank()) {
                    etPasswordAcc.setError("Password harus diisi")
                }
                if (!isEmailValid(getEmailId)) {
                    etEmailAcc.setError("Email tidak valid")
                }

            } else {
                val bundle = Bundle()
                bundle.putString("data1", etNoHpAcc.text.toString())
                bundle.putString("data2", etEmailAcc.text.toString())
                bundle.putString("data3", etPasswordAcc.text.toString())

                val intent = Intent(this, RegisterActivityStep2::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }
    }

    fun isEmailValid(email: CharSequence): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}
