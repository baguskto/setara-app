package com.tupaiaer.bantusaya.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.tupaiaer.bantusaya.fragment.LawFragment
import com.tupaiaer.bantusaya.fragment.PsychologyFragment

class PagerAdapter(fm: FragmentManager): FragmentPagerAdapter(fm){

    private val pages = listOf(
        PsychologyFragment(),
        LawFragment()
    )

    override fun getItem(p0: Int): Fragment {
        return pages[p0]
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Psikologi"
            else -> "Hukum"
        }
    }
}