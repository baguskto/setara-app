package com.tupaiaer.bantusaya.model

import com.google.gson.annotations.SerializedName

data class Report (
    @field:SerializedName("id")
    val id: String?,
    @field:SerializedName("title")
    val title: String?,
    @field:SerializedName("description")
    val description: String?,
    @field:SerializedName("status")
    val status: String?,
//    @field:SerializedName("user_id")
//    val user_id: String?,
    @field:SerializedName("created_at")
    val created_at: String?,
//    @field:SerializedName("updated_at")
//    val updated_at: String?,
    @field:SerializedName("statusPelapor")
    val statusPelapor: String?,
    @field:SerializedName("suspect")
    val suspect: String?,
    @field:SerializedName("crime_scene")
    val crimeScene: String?,
    @field:SerializedName("victim_name")
    val victimName: String?,
    @field:SerializedName("victim_gender")
    val victimGender: String?,
    @field:SerializedName("victim_ages")
    val victimAges: String?,
    @field:SerializedName("victim_address")
    val victimAddress: String?
)