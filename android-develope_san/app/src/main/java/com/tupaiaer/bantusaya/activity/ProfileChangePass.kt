package com.tupaiaer.bantusaya.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.utils.toast
import kotlinx.android.synthetic.main.activity_profile_change_pass.*
import kotlinx.android.synthetic.main.activity_profile_edit.*

class ProfileChangePass : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_change_pass)

        toolbarChangePass.setNavigationIcon(R.drawable.ic_arrow)
        toolbarChangePass.setNavigationOnClickListener {
            onBackPressed()
        }

        btnChangePass.setOnClickListener {
            Snackbar.make(rootProfileEdit, "Fitur mendatang:)", Snackbar.LENGTH_SHORT).show()
        }
    }
}
