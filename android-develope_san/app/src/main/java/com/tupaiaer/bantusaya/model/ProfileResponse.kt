package com.tupaiaer.bantusaya.model

data class ProfileResponse (val result: Boolean, val user: User)

data class User(
    val address: String,
    val admin_desc: Any,
    val birthDate: String,
    val created_at: String,
    val domisili: Any,
    val email: String,
    val gender: String,
    val id: Int,
    val identity_num: Any,
    val identity_pic: IdentityPic,
    val office_num: Any,
    val password_digest: String,
    val phoneNumber: String,
    val profile_pic: ProfilePic,
    val service_time: Any,
    val updated_at: String,
    val user_prev: Boolean,
    val username: String
)

data class IdentityPic(
    val url: Any
)

data class ProfilePic(
    val url: Any
)