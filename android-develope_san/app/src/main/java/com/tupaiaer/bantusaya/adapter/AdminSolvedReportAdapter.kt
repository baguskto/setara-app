package com.tupaiaer.bantusaya.adapter

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.model.Laporan
import kotlinx.android.synthetic.main.report_item.view.*

class AdminSolvedReportAdapter(
    private val reportList: List<Laporan>,
    private val onClickListener: (report: Laporan) -> Unit
) : RecyclerView.Adapter<AdminSolvedReportAdapter.AdminReportHolder>() {

    override fun onCreateViewHolder(group: ViewGroup, viewType: Int): AdminReportHolder {
        val inflater = LayoutInflater.from(group.context)
        val view: View = inflater.inflate(R.layout.report_item, group, false)
        return AdminReportHolder(view)
    }

    override fun getItemCount(): Int = reportList.size

    override fun onBindViewHolder(holder: AdminReportHolder, i: Int) {
        val report = reportList[i]
        holder.bind(report)

        holder.itemView.setOnClickListener {
            onClickListener(report)
        }
    }

    inner class AdminReportHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(report: Laporan) = with(itemView) {
            tvReportId.text = "LAPORAN  ID-" + report.id
            tvReportCategory.text = report.title
            tvReportStatus.text = report.status
//            val userId = report.user_id
            tvReportDate.text = report.created_at
//            val updatedAt = report.updated_at
            val statusPelapor = report.statusPelapor

            if (report.status == "progress" || report.status == "Progress") {
                tvReportStatus.setBackgroundResource(R.drawable.ic_rectangle_progress)
                tvReportStatus.setTextColor(ContextCompat.getColor(context, R.color.white))
            } else if (report.status == "solved" || report.status == "Solved") {
                tvReportStatus.setBackgroundResource(R.drawable.ic_rectangle_solved)
                cvReportItem.setCardBackgroundColor(ContextCompat.getColor(context, R.color.green2))
                tvReportCategory.setTextColor(ContextCompat.getColor(context, R.color.white))
                tvReportDate.setTextColor(ContextCompat.getColor(context, R.color.white))
                tvReportId.setTextColor(ContextCompat.getColor(context, R.color.white))
            }

            if (report.status == "solved" || report.status == "Solved") {
                cvReportItem.visibility = View.VISIBLE
            } else {
                cvReportItem.visibility = View.GONE
                cvReportItem.visibility = View.INVISIBLE
            }
        }
    }
}