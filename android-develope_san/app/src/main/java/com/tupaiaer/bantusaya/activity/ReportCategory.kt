package com.tupaiaer.bantusaya.activity

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.Toast
import com.tupaiaer.bantusaya.Constants.KATEGORI_LAPOR
import com.tupaiaer.bantusaya.Constants.STATUS_PELAPOR
import com.tupaiaer.bantusaya.R
import com.tupaiaer.bantusaya.utils.toast
import kotlinx.android.synthetic.main.activity_report.*
import kotlinx.android.synthetic.main.activity_report_category.*
import kotlinx.android.synthetic.main.activity_report_category.view.*

class ReportCategory : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_category)

        var kategori: String = ""
        val statusPelapor:String = intent.getStringExtra(STATUS_PELAPOR)

        toolbarReportCategory.setNavigationIcon(R.drawable.ic_arrow)
        toolbarReportCategory.setNavigationOnClickListener {
            onBackPressed()
        }


        cvKdrt.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cvKdrt.backgroundTintList = getColorStateList(R.color.colorPrimary)
                tvKdrt.setTextColor(ContextCompat.getColor(this, R.color.white))
                cvPelecehanSeksual.backgroundTintList = getColorStateList(R.color.white)
                tvPs.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanFisik.backgroundTintList = getColorStateList(R.color.white)
                tvKf.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvAncaman.backgroundTintList = getColorStateList(R.color.white)
                tvAncaman.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanAnak.backgroundTintList = getColorStateList(R.color.white)
                tvKpa.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanLainnya.backgroundTintList = getColorStateList(R.color.white)
                tvKl.setTextColor(ContextCompat.getColor(this, R.color.black))
            }

            kategori = tvKdrt.text.toString()
        }

        cvPelecehanSeksual.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cvKdrt.backgroundTintList = getColorStateList(R.color.white)
                tvKdrt.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvPelecehanSeksual.backgroundTintList = getColorStateList(R.color.colorPrimary)
                tvPs.setTextColor(ContextCompat.getColor(this, R.color.white))
                cvKekerasanFisik.backgroundTintList = getColorStateList(R.color.white)
                tvKf.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvAncaman.backgroundTintList = getColorStateList(R.color.white)
                tvAncaman.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanAnak.backgroundTintList = getColorStateList(R.color.white)
                tvKpa.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanLainnya.backgroundTintList = getColorStateList(R.color.white)
                tvKl.setTextColor(ContextCompat.getColor(this, R.color.black))
            }

            kategori = tvPs.text.toString()
        }

        cvKekerasanFisik.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cvKdrt.backgroundTintList = getColorStateList(R.color.white)
                tvKdrt.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvPelecehanSeksual.backgroundTintList = getColorStateList(R.color.white)
                tvPs.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanFisik.backgroundTintList = getColorStateList(R.color.colorPrimary)
                tvKf.setTextColor(ContextCompat.getColor(this, R.color.white))
                cvAncaman.backgroundTintList = getColorStateList(R.color.white)
                tvAncaman.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanAnak.backgroundTintList = getColorStateList(R.color.white)
                tvKpa.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanLainnya.backgroundTintList = getColorStateList(R.color.white)
                tvKl.setTextColor(ContextCompat.getColor(this, R.color.black))
            }

            kategori = tvKf.text.toString()
        }

        cvAncaman.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cvKdrt.backgroundTintList = getColorStateList(R.color.white)
                tvKdrt.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvPelecehanSeksual.backgroundTintList = getColorStateList(R.color.white)
                tvPs.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanFisik.backgroundTintList = getColorStateList(R.color.white)
                tvKf.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvAncaman.backgroundTintList = getColorStateList(R.color.colorPrimary)
                tvAncaman.setTextColor(ContextCompat.getColor(this, R.color.white))
                cvKekerasanAnak.backgroundTintList = getColorStateList(R.color.white)
                tvKpa.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanLainnya.backgroundTintList = getColorStateList(R.color.white)
                tvKl.setTextColor(ContextCompat.getColor(this, R.color.black))
            }

            kategori = tvAncaman.text.toString()
        }

        cvKekerasanAnak.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cvKdrt.backgroundTintList = getColorStateList(R.color.white)
                tvKdrt.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvPelecehanSeksual.backgroundTintList = getColorStateList(R.color.white)
                tvPs.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanFisik.backgroundTintList = getColorStateList(R.color.white)
                tvKf.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvAncaman.backgroundTintList = getColorStateList(R.color.white)
                tvAncaman.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanAnak.backgroundTintList = getColorStateList(R.color.colorPrimary)
                tvKpa.setTextColor(ContextCompat.getColor(this, R.color.white))
                cvKekerasanLainnya.backgroundTintList = getColorStateList(R.color.white)
                tvKl.setTextColor(ContextCompat.getColor(this, R.color.black))
            }

            kategori = tvKpa.text.toString()
        }

        cvKekerasanLainnya.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cvKdrt.backgroundTintList = getColorStateList(R.color.white)
                tvKdrt.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvPelecehanSeksual.backgroundTintList = getColorStateList(R.color.white)
                tvPs.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanFisik.backgroundTintList = getColorStateList(R.color.white)
                tvKf.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvAncaman.backgroundTintList = getColorStateList(R.color.white)
                tvAncaman.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanAnak.backgroundTintList = getColorStateList(R.color.white)
                tvKpa.setTextColor(ContextCompat.getColor(this, R.color.black))
                cvKekerasanLainnya.backgroundTintList = getColorStateList(R.color.colorPrimary)
                tvKl.setTextColor(ContextCompat.getColor(this, R.color.white))
            }

            kategori = tvKl.text.toString()
        }

        btnLanjutReportCategory.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("kategori", kategori)

            if (statusPelapor == "Korban" && kategori.isNotBlank()) {
                bundle.putString(KATEGORI_LAPOR, kategori)
                bundle.putString(STATUS_PELAPOR, statusPelapor)
                val intent = Intent(this, ReportVic::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            }else if(statusPelapor == "Saksi" && kategori.isNotBlank()) {
                bundle.putString(KATEGORI_LAPOR, kategori)
                bundle.putString(STATUS_PELAPOR, statusPelapor)
                val intent = Intent(this, ReportWit::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }
    }
}
