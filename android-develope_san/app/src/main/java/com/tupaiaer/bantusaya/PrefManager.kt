package com.tupaiaer.bantusaya

import android.content.Context
import android.content.SharedPreferences

class PrefManager(context: Context) {

    private val IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch"


    private val sp: SharedPreferences by lazy {
        context.applicationContext.getSharedPreferences(Constants.SP_PREF_NAME, Context.MODE_PRIVATE)
    }

    private val spe: SharedPreferences.Editor by lazy {
        sp.edit()
    }

    fun logOut() {
        spe.clear()
        spe.commit()
    }

    var hasIntro: Boolean
        get() = sp.getBoolean(Constants.SP_HAS_INTRO_SLIDER, false)
        set(value) = spe.putBoolean(Constants.SP_HAS_INTRO_SLIDER, value).apply()

    var token: String?
        get() = sp.getString(Constants.SP_TOKEN, "")
        set(value) {
            spe.putString(Constants.SP_TOKEN, value)
            spe.apply()
        }

    var idUser: String?
        get() = sp.getString(Constants.SP_ID_USER, "")
        set(value) {
            spe.putString(Constants.SP_ID_USER, value)
            spe.apply()
        }

    var name: String?
        get() = sp.getString(Constants.SP_NAME, "")
        set(value) {
            spe.putString(Constants.SP_NAME, value)
            spe.apply()
        }

    var birthdate: String?
        get() = sp.getString(Constants.SP_BIRTHDATE, "")
        set(value) {
            spe.putString(Constants.SP_BIRTHDATE, value)
            spe.apply()
        }

    var address: String?
        get() = sp.getString(Constants.SP_ADDRESS, "")
        set(value) {
            spe.putString(Constants.SP_ADDRESS, value)
            spe.apply()
        }

    var email: String?
        get() = sp.getString(Constants.SP_EMAIL, "")
        set(value) {
            spe.putString(Constants.SP_EMAIL, value)
            spe.apply()
        }

    var phone: String?
        get() = sp.getString(Constants.SP_PHONE, "")
        set(value) {
            spe.putString(Constants.SP_PHONE, value)
            spe.apply()
        }

    var idConversation: String?
        get() = sp.getString(Constants.SP_ID_CONV, "")
        set(value) {
            spe.putString(Constants.SP_ID_CONV, value)
            spe.apply()
        }

    var idUserConversation: String?
        get() = sp.getString(Constants.SP_USER_ID_CONV, "")
        set(value) {
            spe.putString(Constants.SP_USER_ID_CONV, value)
            spe.apply()
        }

    var idInstutionConversation: String?
        get() = sp.getString(Constants.SP_INS_ID_CONV, "")
        set(value) {
            spe.putString(Constants.SP_INS_ID_CONV, value)
            spe.apply()
        }

    var replyBody: String?
        get() = sp.getString(Constants.SP_REPLY_BODY, "")
        set(value) {
            spe.putString(Constants.SP_REPLY_BODY, value)
            spe.apply()
        }

    var createdConversation: String?
        get() = sp.getString(Constants.SP_PHONE, "")
        set(value) {
            spe.putString(Constants.SP_PHONE, value)
            spe.apply()
        }

    var blankReport: String?
        get() = sp.getString(Constants.BLANK_REPORT, "")
        set(value) {
            spe.putString(Constants.BLANK_REPORT, value)
            spe.apply()
        }

    var hasLogin: Boolean
        get() = sp.getBoolean(Constants.SP_HAS_LOGIN, false)
        set(value) = spe.putBoolean(Constants.SP_HAS_LOGIN, value).apply()

    fun setFirstTimeLaunch(isFirstTime: Boolean) {
        spe.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime)
        spe.commit()
    }

    fun isFirstTimeLaunch(): Boolean {
        return sp.getBoolean(IS_FIRST_TIME_LAUNCH, true)
    }

}